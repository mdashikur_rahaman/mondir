<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refundbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('book_id');
            $table->date('refund_date');
            $table->integer('pcs');
            $table->integer('amount');
            $table->string('inv_num');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refundbooks');
    }
}
