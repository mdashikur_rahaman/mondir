<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiveds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dep_id');
            $table->integer('led_id');
            $table->date('date');
            $table->text('description');
            $table->integer('dr');
            $table->integer('cr');
            $table->integer('inv_gen');
            $table->string('tra_num');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiveds');
    }
}
