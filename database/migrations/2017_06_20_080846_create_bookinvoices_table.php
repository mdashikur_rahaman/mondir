<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookinvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookinvoices', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('customer_id')->unsigned();
            $table->string('inv_num');
            $table->string('pay_type');
            $table->integer('total_amount')->unsigned();
            $table->integer('rcv_amount')->unsigned();
            $table->integer('discount')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookinvoices');
    }
}
