@extends('layout.app')
@section('pageheader','Add Information')
@section('content')
<form action="{{route('addinf')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="form-group">
            <label for="">Name Of Department</label>
            <input type="text" required name="dep_name" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Address</label>
            <input type="text" required name="address" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Contact Person</label>
            <input type="text" required name="con_person" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Phone Number</label>
            <input type="number" minlength="11" maxLength="11" required name="phone" class="form-control">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>


@endsection