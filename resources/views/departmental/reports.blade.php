@extends('layout.app')
@section('content')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <h4 class="text-center">INTERNATIONAL SOCIETY FOR KRISHNA CONCIOUSNES (ISKCON)</h4>
            <p class="text-center">Sri Sri Radha Madhav Jew Temple, Jugaltilla, Sylhet			
            </p>
            <p class="text-center">AS AT		
            </p>
            <p class="text-center"><strong> @if($fm==1)
                January
                @elseif($fm==2)
                February
                @elseif($fm==2)
                February
                @elseif($fm==3)
                March
                @elseif($fm==4)
                April
                @elseif($fm==5)
                May
                @elseif($fm==6)
                June
                @elseif($fm==7)
                July
                @elseif($fm==8)
                August
                @elseif($fm==9)
                September
                @elseif($fm==10)
                October
                @elseif($fm==11)
                November
                @elseif($fm==12)
                December
                @endif {{$fy}}</strong> TO <strong> @if($tm==1)
                January
                @elseif($tm==2)
                February
                @elseif($tm==2)
                February
                @elseif($tm==3)
                March
                @elseif($tm==4)
                April
                @elseif($tm==5)
                May
                @elseif($tm==6)
                June
                @elseif($tm==7)
                July
                @elseif($tm==8)
                August
                @elseif($tm==9)
                September
                @elseif($tm==10)
                October
                @elseif($tm==11)
                November
                @elseif($tm==12)
                December
                @endif {{$ty}}</strong></p>
        <table class="table table-striped table">
            <tr>
                <th class="text-center">Date</th>
                <th class="text-center">Roshid Number</th>
                <th class="text-center">Folio Number</th>
                <th class="text-center">Name</th>
                <th class="text-center">Item Type</th>
                <th class="text-center">Amount</th>
            </tr>
            @foreach($reports as $rp)
            <tr>
                <td class="text-center">{{$rp->t_date}}</td>
                <td class="text-center">{{$rp->rosid_num}}</td>
                <td class="text-center">{{$rp->folio_num}}</td>
                <td class="text-center">{{$rp->name}}</td>
                <td class="text-center">{{$rp->item_type}}</td>
                <td class="text-center">{{$rp->amount}}</td>
            </tr>
            @endforeach
             <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="bg-primary">
                <td class="text-center">TOTAL</td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td></td>
                <td class="text-center">{{$total}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td>Total Book</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$tb}}</td>
            </tr>
            <tr>
                <td>Total Para</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$tp}}</td>
            </tr>
            <tr>
                <td>Total Others</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$to}}</td>
            </tr>
            <tr class="bg-success">
                <td></td>
                <td>Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$tb+$tp+$to}}</td>
            </tr>
        </table>
        </div>
    </div>
@endsection