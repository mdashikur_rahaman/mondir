@extends('layout.app')
@section('pageheader','Add Departmental Transection')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<form action="{{route('adddeptrans')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-4">
            <div class="form-group">
            <label for="">Date</label>
            <input type="text" name="t_date" required id="datepicker"class="form-control">
        </div>
            </div>
            <div class="col-lg-8">
             <div class="form-group">
                <label for="">Select Department</label>
                <select name="dep_id" id="" required class="form-control">
                    <option value="">Choose A Department</option>
                   
                        @foreach($deps as $dep)
                            <option value="{{$dep->id}}">{{$dep->dep_name}}</option>
                        @endforeach
                  
                </select>
             </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
            <div class="form-group">
                <label for="">Roshid Number</label>
                <input type="text" required name="rosid_num" class="form-control">
            </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Folio Number</label>
                    <input type="text" required name="folio_num" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Item Type</label>
                    <select name="item_type" required id="" class="form-control">
                        <option value="book">Book</option>
                        <option value="para">Para</option>
                        <option value="other">Other</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" required name="name" class="form-control">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Amount</label>
                    <input type="number" required name="amount" placeholder="Please Enter Amount" class="form-control">
                </div>
            </div>
        </div>   
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>
@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
 $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
    });
  } ); 
 </script> 
@endsection