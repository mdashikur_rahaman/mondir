@extends('layout.app')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
<style>
th,td {
  text-align:center;
}
tr,h4 {
  text-transform:capitalize;
}
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-sm-8 col-sm-offset-2">
    <h3 class="text-center">
      <strong>Sri Sri Radha govinda jew mandir</strong>
    </h3>
    <h5 class="text-center">
      <strong>Wari,Dhaka</strong>
    </h5>
    <h4 class="text-center">
      <strong>Invoice/Bill</strong>
    </h4>
  
    <div class="row">
      <div class="col-sm-8">
        <h4>Name of temple: <strong>{{$customer}}</strong></h4>
        <h4>Address: <strong>{{$address}}</strong></h4>
      </div>
      <div class="col-sm-4">
        <h4>Date: {{$infos[0]->date}}</h4>
        <h4><strong>{{$infos[0]->inv_num}}</strong></h4>
      </div>
      <table class="table table-bordered">
        <tr>
          <th>S.L</th>
          <th>Particulars</th>
          <th>Pcs</th>
          <th>Rate</th>
          <th>Total Amount</th>
        </tr>
        @foreach($items as $key=>$item )
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$item->book}}</td>
          <td>{{$item->pcs}}</td>
          <td>{{$item->sales}}</td>
          <td>{{$item->price}}</td>
        </tr>
        @endforeach
      </table>
      <div class="pull-left">
         <h4> &nbsp; &nbsp; &nbsp; Recieved: <strong>{{$total->rcv}}</strong></h4>
         <h4> &nbsp; &nbsp; &nbsp; Due Amount: <strong>{{$total->due}}</strong></h4> 
        
      </div>
      <div class="pull-right" style="margin-right:20px">
         <h4>Subtotal: <strong>{{$total->total+$total->discount}}</strong></h4>
         <h4>Discount: <strong>{{$total->discount}}</strong></h4> 
        <h4>Grand Total: <strong>{{$total->total}}</strong></h4> 
      </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


@endsection
