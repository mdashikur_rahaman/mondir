@extends('layout.app')
@section('pageheader','Add New Customer')
@section('content')
<form action="{{route('addcustomer')}}" role="form" method="POST" data-parsley-validate>
   {{ csrf_field() }}
   @if (count($errors) > 0)
            <div id="myElem" class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <div class="col-lg-6">
        <div class="form-group">
            <label for="">Name Of Temple</label>
            <input type="text" required name="temple_name" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Address</label>
            <input type="text" required name="address" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Phone Number</label>
            <input type="text" required data-parsley-minlength="11" data-parsley-maxlength="11" name="phone" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Contact Person's Name</label>
            <input type="text" required name="contact_person" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Openning Balance</label>
            <input type="text" required name="openning_bal" class="form-control">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
    </div>
</form>
@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $("#myElem").show().delay(3000).fadeOut();
});
</script>
@endsection