
@extends('layout.app')
@section('pageheader','Purchase Book')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<form action="{{route('addpurchasebook')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">

    @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('message'))
         <div  class="alert alert-success myElem">
            {{session('message')}}
         </div>
         @endif
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                <label for="">Date</label>
                <input type="text" required name="date" class="form-control today" id="datepicker" placeholder="Click For Select">    
            </div>
        </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Challan Number</label>
                    <input type="text" required name="challan_no" class="form-control" placeholder="Input Manually">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Name Of Book</label>
            <select name="book_id" required class="form-control">
                <option value="">Please select a book</option>
            @foreach ($books as $book)
                <option value="{{$book->id}}">{{$book->book_name}}</option>
             @endforeach   
            </select>
            
        </div>
        <div class="form-group">
            <label for="">Number Of Pcs</label>
            <input type="number" required name="pcs" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Purchase Price</label>
            <input type="number" required name="purchases_price" class="form-control" placeholder="Purchase Price Per Unit">
        </div>
        <div class="form-group">
            <label for="">Sales Price</label>
            <input type="number" required name="sales_price" class="form-control" placeholder="Sale Price Per Unit">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>


@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
   $( "#datepicker" ).datepicker({
        changeMonth: true,
      changeYear: true,
      minDate: -7,
      maxDate:0,
      dateFormat: 'yy-mm-dd',
    }).datepicker().datepicker("setDate", new Date());
 $( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
  </script>
@endsection