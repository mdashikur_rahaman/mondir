@extends('layout.app')
@section('pageheader','Sale Books')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
<style>
select,option {
    text-transform:capitalize;
}
</style>
@endsection
@section('content')
<form action="{{route('addsalebook')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
@if(session('status'))
         <div class="alert alert-success myElem">
            {{session('status')}}
         </div>
        @endif
    <div class="col-lg-8" id="sale">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Name Of Temple</label>
                    <select name="customer_id" id="" class="form-control input-large" required>
                        <option value="" selected disabled>Please Select</option>
                        @foreach($customers as $customer)
                        <option value="{{$customer->id}}">{{$customer->temple_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="">Invoice Number</label>
                    @if(empty($inv))
                    <input name="inv_num"readonly type="text" class="form-control" value="INVCBOOK0{{1}}">
                    @else
                    <input name="inv_num" readonly type="text" class="form-control" value="INVCBOOK0{{$inv+1}}">
                    @endif
                    <input type="hidden" name="inv_gen" value="{{$inv+1}}">
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                <label for="">Date</label>
                <input name="date" type="text" id="datepicker" required class="form-control">
                </div>
            </div>
        </div>
       
        <div class="row" v-for="n in products">
            <div class="col-lg-4">
             <div class="form-group">
            <label for="">Name Of Book</label>
            <select name="book_id[]" required class="form-control"  v-model="n.price">
                <option value="0" selected>Please select...</option>
                 @foreach($books as $book)
                        <option value="{{$book->sales_price}}|{{$book->id}}|">{{$book->book_name}}</option>
                        
                @endforeach
            </select>
        </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                <label for="">Number Of Pcs</label>
                <input type="number" required name="pcs[]" class="form-control" v-model="n.pcs">
            </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                <label for="">Price</label>
                <input name="price[]" readonly class="form-control" :value="parseInt(n.price) * n.pcs">
            </div>
            </div>
        </div>
        <p style="display:inline-block;color:green;font-size:30px" @click="addline"><i class="fa fa-plus-square"></i></p>
        <p style="display:inline-block;color:red;font-size:30px" @click="remove"><i class="fa fa-minus-square"></i> </p>
        <div class="row">
            <div class="col-lg-6">
                
            </div>
            <div class="col-lg-6">
                <div class="">
                    <label for="">Subtotal</label>
                    <input  readonly class="form-control" :value="subTotal">
                </div>
                <div class="">
                    <label for="">Discount</label>
                    <input name="discount" type="number" class="form-control" v-model="discount">
                </div>
                <div class="form-group">
                    <label for="">TOTAL(<span class="text-primary">after discount</span>)</label>
                    <input name="total_amount" type="number" readonly class="form-control" :value="grandTotal">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Recieved Amount</label>
                    <input name="rcv_amount" required type="number" class="form-control">
                </div>
            </div>
             <div class="col-lg-6">
                <div class="form-group">
                <label for="">Payment Method</label>
                   <select name="pay_type" required id="" v-model="payType" class="form-control">
                   <option value="cash" selected>Cash</option>
                   <option value="bank">Bank</option>
                   </select>
                </div>
            </div>
           
        </div>
        <div  >
                 <div class="form-group" v-show="payType =='bank'">
                    <label for="">Cheque Number</label>
                    <input name="cheque_num" type="text" class="form-control" placeholder="Cheque number">
                </div>
        </div>
        <p  id="createInvoice"class="btn  btn-success"><a href="{{route('bookinvoice')}}">View Bill</a></p>
        <button type="Save" class="btn  btn-primary">Save</button>
    </div>
</form>


@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
        changeMonth: true,
      changeYear: true,
      minDate: -7,
      maxDate:0,
      dateFormat: 'yy-mm-dd',
    }).datepicker().datepicker("setDate", new Date());
  } );
//vue js 
var sale = new Vue({
    el:'#sale',
    data: {
        discount: 0,
        products:[{ price: 0, pcs:''},{ price: 0, pcs:''}],
        payType:'cash',
    },
    methods:{
        addline: function() {
      this.products.push({ price: 0, pcs:''});
     },

    remove: function() {
      this.products.pop();
    }
    },
   computed: {
    subTotal: function() {
      return this.products.reduce(function(carry, product) {
        return carry + (product.pcs * parseInt(product.price));
      }, 0);
    },
    grandTotal: function() {
      return this.subTotal - this.discount;
    }
  }
});
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});

  </script>
@endsection
