@extends('layout.app')
@section('pageheader','Transfer to Waste Book')
@section('content')
<form action="{{route('atb')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
   <div id="trfbook">
   @if(session('status'))
         <div class="alert alert-success myElem">
            {{session('status')}}
         </div>
        @endif
     <div class="row" v-for="row in rows">
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">Name Of Book</label>
                <select name="book_id[]" required class="form-control">
                    <option value="">please select</option>
                    @foreach($books as $book)
                    <option value="{{$book->id}}">{{$book->book_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="col-lg-2">
            <div class="form-group">
                <label for="">PCS</label>
                <input type="text" name="pcs[]" required class="form-control" placeholder="input manually">
            </div>
        </div>
    </div>
    <p style="display:inline-block;color:green;font-size:30px" @click="addline"><i class="fa fa-plus-square"></i></p>
        <p style="display:inline-block;color:red;font-size:30px" @click="remove"><i class="fa fa-minus-square"></i> </p>
   </div>
   <div class="row">
 <div class="col-lg-2">
    <button type="submit" class="btn btn-primary btn-block">Save</button>
 </div>
</div>
</form>

@endsection

@section('javascript')
<script>
var trfbook = new Vue({
    el:'#trfbook',
    data: {
        rows:1
    },
    methods: {
        addline: function () {
            this.rows +=1;
        },
        remove: function() {
            this.rows -=1;
        }
    }
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
});
</script>
@endsection