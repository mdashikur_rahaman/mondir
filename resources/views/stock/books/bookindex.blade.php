@extends('layout.app')
@section('pageheader','All Book List')
@section('content')

<table class="table table-responsive table-striped">
    <thead>
        <tr>
        <th>Book Name</th>
        <th>Writer</th>
        <th>Origin</th>
        <th>Openning PCS</th>
        <th>Edit</th>
    </tr>
    </thead>
    <tbody>
        @foreach($books as $book)
        <tr>
            <td style="font-size:1.4em">{{$book->book_name}}</td>
            <td style="font-size:1.2em">{{$book->writer}}</td>
            <td style="font-size:1.2em">{{$book->origin}}</td>
             <td style="font-size:1.2em">{{$book->opn_pcs}}</td>
            <td>
                <p class="btn btn-danger">Edit</p>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection