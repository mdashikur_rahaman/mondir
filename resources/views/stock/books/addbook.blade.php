@extends('layout.app')
@section('pageheader','Add New Book')
@section('content')
<form action="{{route('addbook')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">
        @if (count($errors) > 0)
            <div class="alert alert-danger myElem">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('status'))
         <div class="alert alert-success myElem">
            {{session('status')}}
         </div>
        @endif
        <div class="form-group">
            <label for="">Name Of Book</label>
            <input type="text" name="book_name" required data-parsley-minlength="4" data-parsley-maxlength="60" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Writer</label>
            <input type="text" data-parsley-minlength="4" data-parsley-maxlength="60" name="writer" required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Origin</label>
            <input type="text" name="origin" data-parsley-minlength="4" data-parsley-maxlength="10"  required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Openning PCS</label>
            <input type="number" name="opn_pcs" required class="form-control">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>


@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
</script>
@endsection