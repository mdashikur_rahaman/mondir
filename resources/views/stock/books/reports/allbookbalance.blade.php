@extends('layout.app')
@section('pageheader','All Book Balance')
@section('stylesheet')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<style>
tr td {
    text-transform:capitalize;
    text-align:center;
    font-weight:bold;
}
</style>
@endsection
@section('content')
<div class="">
    <table id="example" class="table table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Book Name</th>
                <th>Openning</th>
                <th>Purchases</th>
                <th>Sales</th>
                <th>Refunded</th>
                <th>Wasted</th>
                <th>Balance-PCS</th>
                <th>Purchase Price</th>
                <th>Amount Taka</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach($books as $book)
            <tr>
                <td>{{$book->book_name}}</td>
                <td>{{$book->openning}}</td>
                <td>{{$book->purchase}}</td>
                <td>{{$book->sales}}</td>
                <td>{{$book->refund}}</td>
                <td>{{$book->wasted}}</td>
                <td>{{$book->openning+$book->purchase-$book->sales-$book->wasted+$book->refund}}</td>
                <td>{{$book->purchase_price}}</td>
                <td>{{($book->openning+$book->purchase-$book->sales-$book->wasted+$book->refund)*$book->purchase_price}}</td>
            </tr>
        @endforeach   
        </tbody>
    </table>
</div>
@endsection
@section('javascript')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
