 @extends('layout.app')
@section('pageheader','All Book Balance')
@section('stylesheet')
<style>
select option {
    font-size:16px;
}
</style>
@endsection
@section('content')
<form action="{{route('allbook')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="row">
    <div class="col-md-2">
     <div class="form-group">
            <label for="">Select Month</label>
            <select name="fmonth" required class="form-control">
                <option value="">Please Select</option>
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
                <label for="">Select Year</label>
                <select name="fyear" required class="yearpicker form-control"></select>
            </div>
    </div>
    <div class="col-md-1"><h4><i class="fa fa-arrow-left" aria-hidden="true"></i><i class="fa fa-arrow-right" aria-hidden="true"></i></h4></div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="">Select Month</label>
            <select name="tmonth" required class="form-control">
                <option value="">Please Select</option>
                <option value="1">January</option>
                <option value="2">February</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
                <label for="">Select Year</label>
                <select name="tyear" required class="yearpicker form-control"></select>
            </div>
    </div>
</div>
       <div class="row">
           <div class="col-md-3 col-md-offset-4">
            <input type="submit" value="show report" class="btn btn-primary">
           </div>
       </div>     
        
</form>
@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    for (i = new Date().getFullYear(); i > 2016; i--)
{
    $('.yearpicker').append($('<option />').val(i).html(i));
}
});
</script>
@endsection