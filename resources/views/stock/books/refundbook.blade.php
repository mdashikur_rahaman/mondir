
@extends('layout.app')
@section('pageheader','Refund Book')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<div id="rbook">
    <form action="{{route('arb')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">
    @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    @if(session('status'))
         <div class="alert alert-success myElem">
            {{session('status')}}
         </div>
        @endif
        <div class="row" id="rb">
            <div class="col-lg-6">
                <div class="form-group">
                <label for="">Refund Date</label>
                <input type="text" required name="refund_date" class="form-control today" id="datepicker" placeholder="Click For Select">    
            </div>
        </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Invoice Number</label>
                    <input type="text" required name="inv_num" class="form-control" placeholder="Input Manually">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Name Of Temple</label>
            <select name="customer_id" required class="form-control">
                <option value="">Please select a book</option>
            @foreach ($customers as $customer)
                <option value="{{$customer->id}}">{{$customer->temple_name}}</option>
             @endforeach   
            </select>
            
        </div>
       <div class="row" v-for="item in items">
           <div class="col-lg-6">
                <div class="form-group">
                <label for="">Name Of Book</label>
                <select name="book_id" required class="form-control">
                    <option value="">Please select a book</option>
                    @foreach ($books as $book)
                        <option value="{{$book->id}}">{{$book->book_name}}</option>
                    @endforeach   
                </select>
            </div>
        
           </div>
           <div class="col-lg-6">
                <div class="form-group">
                <label for="">Number Of Pcs</label>
                <input type="number" required name="pcs" class="form-control">
            </div>
           </div>
       </div>
       <p style="display:inline-block;color:green;font-size:30px" @click="addline"><i class="fa fa-plus-square"></i></p>
        <p style="display:inline-block;color:red;font-size:30px" @click="remove"><i class="fa fa-minus-square"></i> </p>
        <div class="form-group">
            <label for="">Amount</label>
            <input type="number" required name="amount" class="form-control" placeholder="Input Manually">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>

</div>

@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
    jQuery(document).ready(function($){
        $( "#datepicker" ).datepicker({
                changeMonth: true,
            changeYear: true,
            minDate: -7,
            maxDate:0,
            dateFormat: 'yy-mm-dd',
        }).datepicker().datepicker("setDate", new Date());
    });
   
 var rbook = new Vue({
    el:'#rbook',
    data: {
        items:1
    },
    methods: {
        addline: function () {
            this.items +=1;
        },
        remove: function() {
            this.items -=1;
        }
    }

});
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
  </script>
@endsection