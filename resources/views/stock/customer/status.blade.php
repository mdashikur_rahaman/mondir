@extends('layout.app')
@section('pageheader','Customer Status')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
<style>
td {
    text-transform:capitalize;
}
</style>
@endsection
@section('content')
<form action="{{route('status')}}" role="form" method="POST">
{{ csrf_field() }}
   <div class="row">
       <div class="col-sm-8 col-sm-offset-2">
       <table class="table">
        <tr>
            <th>Temple Name</th>
            <th>Bill Amount</th>
            <th>Received</th>
            <th>Refunded</th>
            <th>Due Balance</th>
            
        </tr>
        @foreach($infos as $info)
            <tr>
                <td>{{$info->customer}}</td>
                <td>{{$info->total}}</td>
                <td>{{$info->rcv}}</td>
                <td>{{$info->refund}}</td>
                <td>{{$info->bal + $info->total - $info->rcv -$info->refund}}</td>
                
            </tr>
        @endforeach
       </table>
       </div>
   </div>
</form>


@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>

@endsection
