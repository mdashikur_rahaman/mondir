@extends('layout.app')
@section('pageheader','Sale Paras')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<form action="{{route('addsalepara')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-8" id="sale">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Name Of Temple</label>
                    <select name="customer_id" required class="form-control input-large">
                        <option value="" selected disabled>Please Select</option>
                        @foreach($customers as $customer)
                        <option value="{{$customer->id}}">{{$customer->temple_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="">Invoice Number</label>
                    @if(empty($inv))
                    <input name="inv_num"readonly type="text" class="form-control" value="INVPARA0{{1}}">
                    @else
                    <input name="inv_num" readonly type="text" class="form-control" value="INVPARA0{{$inv+1}}">  
                    @endif
                    <input type="hidden" name="inv_gen" value="{{$inv+1}}">
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                <label for="">Date</label>
                <input name="date" required type="text" id="datepicker" class="form-control">
                </div>
            </div>
        </div>
       
        <div class="row" v-for="n in products">
            <div class="col-lg-4">
             <div class="form-group">
            <label for="">Name Of Para</label>
            <select name="item_id[]" class="form-control" required v-model="n.price">
                <option value="0" selected>Please select...</option>
                 @foreach($paras as $para)
                        <option value="{{$para->sales_price}}|{{$para->id}}|">{{$para->para_name}}</option>
                        
                @endforeach
            </select>
        </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                <label for="">Number Of Pcs</label>
                <input type="number" name="pcs[]" required class="form-control" v-model="n.pcs">
            </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                <label for="">Price</label>
                <input name="price[]" readonly class="form-control" :value="parseInt(n.price) * n.pcs">
            </div>
            </div>
        </div>
        <p style="display:inline-block;color:green;font-size:30px" @click="addline"><i class="fa fa-plus-square"></i></p>
        <p style="display:inline-block;color:red;font-size:30px" @click="remove"><i class="fa fa-minus-square"></i> </p>
        <div class="row">
            <div class="col-lg-6">
                
            </div>
            <div class="col-lg-6">
                <div class="">
                    <label for="">Subtotal</label>
                    <input  readonly class="form-control" :value="subTotal">
                </div>
                <div class="">
                    <label for="">Discount</label>
                    <input type="number" name="discount" class="form-control" v-model="discount">
                </div>
                <div class="form-group">
                    <label for="">TOTAL(<span class="text-primary">after discount</span>)</label>
                    <input name="total_amount" type="number" readonly class="form-control" :value="grandTotal">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Recieved Amount</label>
                    <input name="rcv_amount" required type="number" class="form-control">
                </div>
            </div>
             <div class="col-lg-6">
                <div class="form-group">
                <label for="">Payment Method</label>
                   <select name="pay_type" id="" v-model="payType" class="form-control">
                   <option value="cash" selected>Cash</option>
                   <option value="bank">Bank</option>
                   </select>
                </div>
            </div>
           
        </div>
        <div v-show="payType == 'bank' " >
                 <div class="form-group">
                    <label for="">Cheque Number</label>
                    <input name="cheque_num" type="number" class="form-control" placeholder="Sale Price Per Unit">
                </div>
        </div>
        <p class="btn  btn-success"> <a href="{{route('parainvoice')}}"> View Bill</a></p>
        <button type="submit" class="btn  btn-primary">Submit</button>
    </div>
</form>


@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
 $( function() {
      $( "#datepicker" ).datepicker({
        changeMonth: true,
      changeYear: true,
      minDate: -7,
      maxDate:0,
      dateFormat: 'yy-mm-dd',
    }).datepicker().datepicker("setDate", new Date());
  } ); 
//vue js 
var sale = new Vue({
    el:'#sale',
    data: {
        discount: 0,
        products:[],
        payType:'cash',
    },
    methods:{
        addline: function() {
      this.products.push({ price: 0, pcs: 1});
     },

    remove: function() {
      this.products.pop();
    }
    },
   computed: {
    subTotal: function() {
      return this.products.reduce(function(carry, product) {
        return carry + (product.pcs * parseInt(product.price));
      }, 0);
    },
    grandTotal: function() {
      return this.subTotal - this.discount;
    }
  }
});


  </script>
@endsection
