
@extends('layout.app')
@section('pageheader','Refund Para')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<form action="{{route('arp')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">
    @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                <label for="">Refund Date</label>
                <input type="text" required name="refund_date" class="form-control today" id="datepicker" placeholder="Click For Select">    
            </div>
        </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Invoice Number</label>
                    <input type="text" required name="inv_num" class="form-control" placeholder="Input Manually">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Name Of Temple</label>
            <select name="customer_id" required class="form-control">
                <option value="">Please select a Customer</option>
            @foreach ($customers as $customer)
                <option value="{{$customer->id}}">{{$customer->temple_name}}</option>
             @endforeach   
            </select>
            
        </div>
        <div class="form-group">
            <label for="">Name Of Para</label>
            <select name="para_id" required class="form-control">
                <option value="">Please select a Para</option>
            @foreach ($paras as $para)
                <option value="{{$para->id}}">{{$para->para_name}}</option>
             @endforeach   
            </select>
            
        </div>
        <div class="form-group">
            <label for="">Number Of Pcs</label>
            <input type="number" required name="pcs" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Amount</label>
            <input type="number" required name="amount" class="form-control" placeholder="Input Manually">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
    </div>
</form>


@endsection
@section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
   $( "#datepicker" ).datepicker({
        changeMonth: true,
      changeYear: true,
      minDate: -7,
      maxDate:0,
      dateFormat: 'yy-mm-dd',
    }).datepicker().datepicker("setDate", new Date());
 
  </script>
@endsection