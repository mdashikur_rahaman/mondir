@extends('layout.app')
@section('pageheader','All para List')
@section('content')

<table class="table table-responsive table-striped">
    <thead>
        <tr>
        <th>Para Name</th>
        <th>Purchases From</th>
        <th>Origin</th>
        <th>Openning PCS</th>
        <th>Edit</th>
    </tr>
    </thead>
    <tbody>
        @foreach($paras as $para)
        <tr>
            <td style="font-size:1.4em">{{$para->para_name}}</td>
            <td style="font-size:1.2em">{{$para->p_from}}</td>
            <td style="font-size:1.2em">{{$para->origin}}</td>
             <td style="font-size:1.2em">{{$para->opn_pcs}}</td>
            <td>
                <p class="btn btn-danger">Edit</p>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection