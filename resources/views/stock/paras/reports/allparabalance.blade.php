@extends('layout.app')
@section('pageheader','All Para Balance')
@section('stylesheet')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<style>
tr td {
    text-transform:capitalize;
    text-align:center;
    font-weight:bold;
}
</style>
@endsection
@section('content')
<div class="row">
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Para Name</th>
                <th>Openning</th>
                <th>Purchases</th>
                <th>Sales</th>
                <th>Refunded</th>
                <th>Wasted</th>
                <th>Balance-PCS</th>
                <th>Purchase Price</th>
                <th>Amount Taka</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Para Name</th>
                <th>Openning</th>
                <th>Purchases</th>
                <th>Sales</th>
                <th>Refunded</th>
                <th>Wasted</th>
                <th>Balance-PCS</th>
                <th>Purchase Price</th>
                <th>Amount Taka</th>
            </tr>
        </tfoot>
        <tbody>
        @foreach($paras as $para)
            <tr>
                <td>{{$para->para_name}}</td>
                <td>{{$para->openning}}</td>
                <td>{{$para->purchase}}</td>
                <td>{{$para->sales}}</td>
                <td>{{$para->refund}}</td>
                <td>{{$para->wasted}}</td>
                <td>{{$para->openning+$para->purchase-$para->sales-$para->wasted+$para->refund}}</td>
                <td>{{$para->purchase_price}}</td>
                <td>{{($para->openning+$para->purchase-$para->sales-$para->wasted+$para->refund)*$para->purchase_price}}</td>
            </tr>
        @endforeach   
        </tbody>
    </table>
</div>
@endsection
@section('javascript')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
