@extends('layout.app')
@section('pageheader','Add New Para')
@section('content')
<form action="{{route('addpara')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    <div class="col-lg-6">
         @if (count($errors) > 0)
            <div class="alert alert-danger myElem">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('status'))
         <div class="alert alert-success myElem">
            {{session('status')}}
         </div>
        @endif
        <div class="form-group">
            <label for="">Name Of Para</label>
            <input type="text" name="para_name" required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Purchase From</label>
            <input type="text" name="p_from" required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Origin</label>
            <input type="text" name="origin" required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Openning PCS</label>
            <input type="number" name="opn_pcs" required class="form-control">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
    </div>
</form>
@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
</script>
@endsection