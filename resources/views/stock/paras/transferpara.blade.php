@extends('layout.app')
@section('pageheader','Transfer to Waste Para')
@section('content')
<form action="{{route('atp')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
   <div id="trfpara">
     <div class="row" v-for="row in rows">
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">Name Of Para</label>
                <select name="para_id[]" required class="form-control">
                    <option value="">Please Select</option>
                    @foreach($paras as $para)
                    <option value="{{$para->id}}">{{$para->para_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="">PCS</label>
                <input type="text" name="pcs[]" required class="form-control" placeholder="input manually">
            </div>
        </div>
    </div>
    <p style="display:inline-block;color:green;font-size:30px" @click="addline"><i class="fa fa-plus-square"></i></p>
        <p style="display:inline-block;color:red;font-size:30px" @click="remove"><i class="fa fa-minus-square"></i> </p>
   </div>
   <div class="row">
 <div class="col-lg-2">
    <button type="submit" class="btn btn-primary btn-block">Save</button>
 </div>
</div>
</form>

@endsection

@section('javascript')
<script>
var trfpara = new Vue({
    el:'#trfpara',
    data: {
        rows:1
    },
    methods: {
        addline: function () {
            this.rows +=1;
        },
        remove: function() {
            this.rows -=1;
        }
    }

});
</script>
@endsection