<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ISKCON- @yield('title') page</title>
<link rel="stylesheet" href="{{asset('css/parsley.css')}}">
<link href="{{asset('css/app.css')}}" rel="stylesheet">
 @yield('stylesheet')
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">ISKCON SOFTWARE</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            <!-- Authentication Links -->
             @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
                
            @else
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                         {{ Auth::user()->name }}
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                         </form>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            @endif
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#!"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Stock<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('customer')}}">Add Customer</a>
                                </li>
                                <li>
                                    <a href="#!">Add New <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{route('book')}}">Add New Book</a>
                                        </li>
                                        <li>
                                            <a href="{{route('para')}}">Add New Para</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#!">Purchase <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{route('purchasebook')}}">Purchases Of Book</a>
                                        </li>
                                        <li>
                                            <a href="{{route('purchasepara')}}">Purchases Of Para</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#">Sales<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{route('salebook')}}">Sales Of Book</a>
                                        </li>
                                        <li>
                                            <a href="{{route('salepara')}}">Sales Of Para</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#">Refund <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{route('rb')}}">Refund Of Book</a>
                                        </li>
                                        <li>
                                            <a href="{{route('rp')}}">Refund Of Para</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#">Report <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{route('bookinq')}}">All book Balance</a>
                                        </li>
                                        <li>
                                            <a href="{{route('parainq')}}">All Para balance</a>
                                        </li>
                                        <li>
                                            <a href="{{route('stat')}}">Customer Status</a>
                                        </li>
                                        <li>
                                            <a href="#">Invoice Wise Report </a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#">Transfer to Waste<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{route('tb')}}">Book</a>
                                        </li>
                                        <li>
                                            <a href="{{route('tp')}}">Para</a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Accounts<span class="fa arrow"></span></a></a>
                            <ul class="nav nav-second-level">
                                        <li>
                                            <a href="{{route('purchasebook')}}">Add New<span class="fa arrow"></span></a></a>
                                            <ul class="nav nav-third-level">
                                                <li>
                                                    <a href="{{route('dep')}}">Add New Department</a>
                                                </li>
                                                <li>
                                                    <a href="{{route('led')}}">Add New Ledger</a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="{{route('tran')}}">Transection</a>
                                        </li>
                                        <li>
                                            <a href="{{route('alldep')}}">All Department recieved and payment </a>
                                        </li>
                                        <li>
                                            <a href="{{route('depwise')}}">Department Wise recieved and payment </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ledwise')}}">Ledger Wise recieved and payment </a>
                                        </li>
                                        <li>
                                            <a href="{{route('fund')}}">Fund Statement</a>
                                        </li>
                                        <li>
                                            <a href="{{route('cashb')}}">Cash-book Report </a>
                                        </li>
                                        <li>
                                            <a href="{{route('bankb')}}">Bank-book Report </a>
                                        </li>
                                    </ul>
                        </li>
                        <li>
                            <a href="#!"><i class="fa fa-edit fa-fw"></i> Department's Account<span class="fa arrow"></span></a></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('depcreate')}}">Add Information</a>
                                </li>
                                <li>
                                    <a href="{{route('deptrans')}}">Transection</a>
                                </li>
                                <li>
                                    <a href="{{route('reportq')}}">Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Devotee<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('devotee.create')}}">Add New Devotee</a>
                                </li>
                                <li>
                                    <a href="{{route('devotee.index')}}">Devotee List</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i>Super Admin<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">General Info</a>
                                </li>
                                <li>
                                    <a href="login.html">Permission</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('pageheader')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              @yield('content')  
            </div>
            <!-- /.row -->
          
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    <!-- Custom Theme JavaScript -->
     <script src="{{asset('js/vue.js')}}"></script>
     <script src="{{asset('js/app.js')}}"></script>
     <script src="{{asset('js/parsley.min.js')}}"></script>
     <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    @yield('javascript')

</body>

</html>
