@extends('layout.app')
@section('pageheader','Add New Ledger')
@section('content')
<form action="{{route('addled')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
    @if(session('status'))
         <div  class="alert alert-success myElem">
            {{session('status')}}
         </div>
    @endif
    @if (count($errors) > 0)
            <div class="alert alert-danger myElem">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <div class="col-lg-6">
        <div class="form-group">
            <label for="">Name Of Ledger</label>
            <input type="text" required name="led_name" value="{{ old('led_name') }}"class="form-control">
        </div>
        <div class="form-group">
            <label>Ledger Type</label>
            <select name="led_type" required value="{{ old('led_type') }}"class="form-control">
                <option value="">Please Select</option>
                <option value="income">Income</option>
                <option value="expense">Expenses</option>
                <option value="bank">Bank</option>
                <option value="cash">Cash</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Openning Balance</label>
           <input type="text" name="op_bal" value="{{old('op_bal')}}"  class="form-control">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>


@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
</script>
@endsection