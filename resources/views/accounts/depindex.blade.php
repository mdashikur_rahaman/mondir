@extends('layout.app')
@section('pageheader','All Department List')
@section('content')

<table class="table table-responsive table-striped">
    <thead>
        <tr>
        <th>Department Name</th>
        <th>Cash Balance</th>
        <th>Bank Balance</th>
        <th>Edit</th>
    </tr>
    </thead>
    <tbody>
        @foreach($departments as $department)
        <tr>
            <td style="font-size:1.4em">{{$department->dep_name}}</td>
            <td style="font-size:1.2em">{{$department->op_bal_cash}}</td>
            <td style="font-size:1.2em">{{$department->op_bal_bank}}</td>
            <td>
                <p class="btn btn-danger">Edit</p>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection