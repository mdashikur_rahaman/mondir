@extends('layout.app')
@section('pageheader','Add New Contra')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<form action="{{route('addcontra')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
@if(session('message'))
         <div id="myElem" class="alert alert-success">
            {{session('message')}}
         </div>
    @endif
    <div class="col-lg-11" id="tran">
        <div class="row">
            
            <div class="col-lg-3">
            <label for="">Transection Number</label>
             @if(empty($tra))
                    <input name="tra_num"readonly required type="text" class="form-control" value="CONTR0{{1}}">
                    @else
                    <input name="tra_num" readonly required type="text" class="form-control" value="CONTR0{{$tra+1}}">
                    @endif
            </div>
            <input type="hidden" name="inv_gen" value="{{$tra+1}}">
            <div class="col-lg-2">
              <p>
                <label for="">Date</label>
                <input type="text" name="date" required readonly class="form-control"  id="datepicker" placeholder="Click For Select"> 
              </p>
            </div>
        </div> 
        {{-- Row ends here --}}
        {{-- journal start here --}}
        <div>
         <div class="row">
          <table class="table table-responsive">
          <tr>
            <th>Department</th>
            <th>Ledger</th>
            <th>Description</th>  
            <th>Payment</th>
            <th>Received</th>
          </tr>
          <tr v-for="journal in journals">
              <td>
                <select class="form-control" required name="dep_id[]">
                <option value="">Please Select</option>
                @foreach($departments as $department)
                    <option value="{{$department->id}}">{{$department->dep_name}}</option>
                @endforeach    
                </select>
              </td>
              <td>
              <select class="form-control" required name="led_id[]">
                <option value="">Please Select</option>
                   @foreach($ledgers as $ledger)
                    <option value="{{$ledger->id}}">{{$ledger->led_name}}</option>
                @endforeach 
                </select>
              </td>
              <td><textarea required name="description[]"  class="form-control"></textarea></td>
              <td><input type="number"  name="dr[]" class="form-control" v-model="journal.dr"></td>
              <td><input type="number" name="cr[]"  class="form-control" v-model="journal.cr"></td>
          </tr>
          
          </table>
          <p  class="col-lg-3 pull-right btn btn-danger"v-if="bal==false">Unbalanced</p>
          <p  class="col-lg-3 pull-right btn btn-success"v-if="bal==true">Balanced</p>
         </div>
        </div>
        <br>
        <button type="submit" class="btn btn-large btn-primary"v-if="bal==true">Post</button>
    </div>
</form>


@endsection
@section('javascript')
{{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
   $( function() {
    $( "#datepicker" ).datepicker({
        changeMonth: true,
      changeYear: true,
      minDate: -7,
      maxDate:0,
      dateFormat: 'yy-mm-dd',
    }).datepicker().datepicker("setDate", new Date());
    $("#myElem").show().delay(3000).fadeOut();
  } );
//vue js 
var tran =  new Vue({
    el:'#tran',
    data: {
       journals:[{dr:'',cr:''},{dr:'',cr:''}]
    },
    computed:{
        bal: function () {
            if(this.journals[0].dr !== this.journals[1].cr){
                return false;
            }else if(this.journals[0].cr !== this.journals[1].dr){
                return false;
            }else{
                return true;
            }
        }
    }
  
});

</script>

@endsection