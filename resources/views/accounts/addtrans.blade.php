@extends('layout.app')
{{-- @section('pageheader','Add New transection') --}}
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
       <div class="col-lg-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="h3">ADD NEW PAYMENT</div>
                            <div>In Transection</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('payment')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Click To Open</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <div class="col-lg-3">
        <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="h3">ADD NEW RECEIVED</div>
                            <div>In Transection</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('received')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Click To Open</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
        <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="h3">ADD NEW JOURNAL</div>
                            <div>In Transection</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('journal')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Click To Open</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
        <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="h3">ADD NEW CONTRA</div>
                            <div>In Transection</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('contra')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Click To Open</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection