@extends('layout.app')
@section('pageheader','Add New Department')
@section('content')
<form action="{{route('adddep')}}" role="form" method="POST" data-parsley-validate>
{{ csrf_field() }}
     @if(session('status'))
         <div class="alert alert-success myElem">
            {{session('status')}}
         </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger myElem">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <div class="col-lg-6">
        <div class="form-group">
            <label for="">Name Of Department</label>
            <input type="text" name="dep_name" value="{{old('dep_name')}}"required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Openning Balance <strong class="text-primary">Cash</strong></label>
            <input type="number" name="op_bal_cash" value="{{old('op_bal_cash')}}" required class="form-control">
        </div>
        <div class="form-group">
            <label for="">Openning Balance <strong class="text-primary">Bank</strong></label>
            <input type="number" name="op_bal_bank" value="{{old('op_bal_bank')}}" required class="form-control">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
</form>
@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $(".myElem").show().delay(3000).fadeOut();
});
</script>
@endsection