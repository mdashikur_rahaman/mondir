@extends('layout.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <img src="{{asset('logo.png')}}" style="margin-left:40%;height:40px;width:160px">
            <h4 class="text-center">INTERNATIONAL SOCIETY FOR KRISHNA CONCIOUSNES (ISKCON)</h4>
            <p class="text-center">Sri Sri Radha Madhav Jew Temple, Jugaltilla, Sylhet			
            </p>
            <p class="text-center">AS AT		
            </p>
            <p class="text-center"><strong> @if($fm==1)
                January
                @elseif($fm==2)
                February
                @elseif($fm==2)
                February
                @elseif($fm==3)
                March
                @elseif($fm==4)
                April
                @elseif($fm==5)
                May
                @elseif($fm==6)
                June
                @elseif($fm==7)
                July
                @elseif($fm==8)
                August
                @elseif($fm==9)
                September
                @elseif($fm==10)
                October
                @elseif($fm==11)
                November
                @elseif($fm==12)
                December
                @endif {{$fy}}</strong> TO <strong> @if($tm==1)
                January
                @elseif($tm==2)
                February
                @elseif($tm==2)
                February
                @elseif($tm==3)
                March
                @elseif($tm==4)
                April
                @elseif($tm==5)
                May
                @elseif($tm==6)
                June
                @elseif($tm==7)
                July
                @elseif($tm==8)
                August
                @elseif($tm==9)
                September
                @elseif($tm==10)
                October
                @elseif($tm==11)
                November
                @elseif($tm==12)
                December
                @endif {{$ty}}</strong></p>
            <p class="text-center">RECEPT & PAYMENT ACCOUNTS</p>
            <h4 class="text-center">{{$dep}}</h4>
        <table class="table table-striped table text-left">
            <tr>
                <th class="text-left">Particulars</th>
                <th class="text-left">Payment</th>
                <th class="text-left">Received</th>
            </tr>
            <tr>
                <td>Openning Balance</td>
                <td>
                </td>
                <td class="text-left">{{$fopb}}</td>
            </tr>
            <tr>
            @foreach($ledinfos as $li)
                <td>{{$li->ledname}}</td>
                <td>{{$li->dr}}</td>
                <td >{{$li->cr}}</td>
            </tr>
            @endforeach
            <tr>
                <td>Closing Balance</td>
                <td>{{$finalclosing}}</td>
                <td></td>
            </tr>
                <td>Total</td>
                <td>{{$total}}</td>
                <td>{{$total}}</td>
            </tr>
            {{-- <tr>
                <td>Closing Balance Bank</td>
                <td></td>
                <td></td>
            </tr> --}}
        </table>
        </div>
    </div>
</div>
@endsection