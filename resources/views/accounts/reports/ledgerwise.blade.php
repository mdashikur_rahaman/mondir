@extends('layout.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h4 class="text-center">INTERNATIONAL SOCIETY FOR KRISHNA CONCIOUSNES (ISKCON)</h4>
            <p class="text-center">Sri Sri Radha Madhav Jew Temple, Jugaltilla, Sylhet			
            </p>
            <p class="text-center">AS AT			
            </p>
            <p class="text-center"><strong> @if($fm==1)
                January
                @elseif($fm==2)
                February
                @elseif($fm==2)
                February
                @elseif($fm==3)
                March
                @elseif($fm==4)
                April
                @elseif($fm==5)
                May
                @elseif($fm==6)
                June
                @elseif($fm==7)
                July
                @elseif($fm==8)
                August
                @elseif($fm==9)
                September
                @elseif($fm==10)
                October
                @elseif($fm==11)
                November
                @elseif($fm==12)
                December
                @endif {{$fy}}</strong> TO <strong> @if($tm==1)
                January
                @elseif($tm==2)
                February
                @elseif($tm==2)
                February
                @elseif($tm==3)
                March
                @elseif($tm==4)
                April
                @elseif($tm==5)
                May
                @elseif($tm==6)
                June
                @elseif($tm==7)
                July
                @elseif($tm==8)
                August
                @elseif($tm==9)
                September
                @elseif($tm==10)
                October
                @elseif($tm==11)
                November
                @elseif($tm==12)
                December
                @endif {{$ty}}</strong></p>
            <h4 class="text-center">RECEIPT & PAYMENT ACCOUNT HEAD WISE</h4>
            
            <div class="col-sm-4">
                <h5><strong>Openning Balance Cash: {{$tob}}</strong></h5>
                <h5><strong>Openning Balance Bank: {{$toc}}</strong></h5>
            </div>
       
        <table class="table table-striped table">
            <tr>
                <th class="text-center">Ledgers</th>
                <th class="text-center">Payment</th>
                <th class="text-center">Receipt</th>
            </tr>
            <tr>
                <td>Openning Balance</td>
                <td></td>
                <td class="text-center">{{$tob+$toc}}</td>
            </tr>
            
            @foreach($leds as $led)
            <tr>
                <td>{{$led->led_name}}</td>
                <td class="text-center">
                @if($led->dr>0)
                {{$led->dr}}
                @else
                -
                @endif
                </td>
                <td class="text-center">
                 @if($led->cr>0)
                {{$led->cr}}
                @else
                -
                @endif
                </td>
            </tr>
            @endforeach
            <tr>
                <td>Closing Balance</td>
                <td class="text-center">{{$tcc+$tcb}}</td>
                <td></td>
            </tr>
        </table>
        <div class="col-sm-4">
                <h5><strong>Closing Balance Cash: {{$tcc}}</strong></h5>
                <h5><strong>Closing Balance Bank: {{$tcb}}</strong></h5>
            </div>
        </div>
    </div>
</div>
@endsection