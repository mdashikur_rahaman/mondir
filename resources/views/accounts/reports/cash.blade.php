@extends('layout.app')
@section('content')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <img src="{{asset('logo.png')}}" style="margin-left:40%;height:40px;width:160px">
            <h4 class="text-center">INTERNATIONAL SOCIETY FOR KRISHNA CONCIOUSNES (ISKCON)</h4>
            <p class="text-center">Sri Sri Radha Madhav Jew Temple, Jugaltilla, Sylhet			
            </p>
           <p class="text-center">AS AT		
            </p>
            <p class="text-center"><strong> @if($fm==1)
                January
                @elseif($fm==2)
                February
                @elseif($fm==2)
                February
                @elseif($fm==3)
                March
                @elseif($fm==4)
                April
                @elseif($fm==5)
                May
                @elseif($fm==6)
                June
                @elseif($fm==7)
                July
                @elseif($fm==8)
                August
                @elseif($fm==9)
                September
                @elseif($fm==10)
                October
                @elseif($fm==11)
                November
                @elseif($fm==12)
                December
                @endif {{$fy}}</strong> TO <strong> @if($tm==1)
                January
                @elseif($tm==2)
                February
                @elseif($tm==2)
                February
                @elseif($tm==3)
                March
                @elseif($tm==4)
                April
                @elseif($tm==5)
                May
                @elseif($tm==6)
                June
                @elseif($tm==7)
                July
                @elseif($tm==8)
                August
                @elseif($tm==9)
                September
                @elseif($tm==10)
                October
                @elseif($tm==11)
                November
                @elseif($tm==12)
                December
                @endif {{$ty}}</strong></p>
            <p class="text-center">CASH BOOK</p>
        <table class="table table-striped table text-left">
            <tr>
                <th class="text-left">Date</th>
                <th class="text-left">Transaction</th>
                <th class="text-left">Department</th>
                <th class="text-left">Ledger</th>
                <th class="text-left">Descriptions</th>
                <th class="text-left">Payment</th>
                <th class="text-left">Reciept</th>
            </tr>
            <tr>
                <td>Openning Balance</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$tobc}}</td>
            </tr>
            @foreach($cbooks as $cb)
            <tr>
                <td>{{date("d-m-Y", strtotime($cb->t_date))}}</td>
                <td>{{$cb->tra_num}}</td>
                <td>{{$cb->dep}}</td>
                <td>{{$cb->led}}</td>
                <td>{{$cb->d}}</td>
                <td>{{$cb->cr}}</td>
                <td>{{$cb->dr}}</td>
            </tr>
            @endforeach
    <tr>
                <td>Closing Balance</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$closing}}</td>
                <td></td>
            </tr>
        </table>
        </div>
    </div>
@endsection