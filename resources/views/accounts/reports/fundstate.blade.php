@extends('layout.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h4 class="text-center">INTERNATIONAL SOCIETY FOR KRISHNA CONCIOUSNES (ISKCON)</h4>
            <p class="text-center">Sri Sri Radha Madhav Jew Temple, Jugaltilla, Sylhet			
            </p>
            <p class="text-center">AS AT		
            </p>
            <p class="text-center"><strong> @if($fm==1)
                January
                @elseif($fm==2)
                February
                @elseif($fm==2)
                February
                @elseif($fm==3)
                March
                @elseif($fm==4)
                April
                @elseif($fm==5)
                May
                @elseif($fm==6)
                June
                @elseif($fm==7)
                July
                @elseif($fm==8)
                August
                @elseif($fm==9)
                September
                @elseif($fm==10)
                October
                @elseif($fm==11)
                November
                @elseif($fm==12)
                December
                @endif {{$fy}}</strong> TO <strong> @if($tm==1)
                January
                @elseif($tm==2)
                February
                @elseif($tm==2)
                February
                @elseif($tm==3)
                March
                @elseif($tm==4)
                April
                @elseif($tm==5)
                May
                @elseif($tm==6)
                June
                @elseif($tm==7)
                July
                @elseif($tm==8)
                August
                @elseif($tm==9)
                September
                @elseif($tm==10)
                October
                @elseif($tm==11)
                November
                @elseif($tm==12)
                December
                @endif {{$ty}}</strong></p>
            <p class="text-center">FUND STATEMENT</p>
        <table class="table">
            <tr>
                <td>Openning Balance Cash</td>
                <td>{{$obc}}</td>
            </tr>
            <tr>
                <td>Openning Balance Bank</td>
                <td>{{$obb}}</td>
            </tr>
        </table>
        <table class="table table-striped table">
            <tr>
                <th class="text-center">Particulars</th>
                <th class="text-center">Excess of Income Over Expenditure</th>
                <th class="text-center">Excess of Expenses Over Income</th>
            </tr>
            <tr>
                <td>Openning Balance</td>
                <td class="text-center">{{$obc+$obb}}</td>
                <td></td>
            </tr>
            @php
                $income = 0;
                $expense=0;
            @endphp
            @foreach($funds as $fund)
            <tr>
                <td>{{$fund->dep}}</td>
                <td class="text-center">
                @if($fund->dr < $fund->cr)
                {{$fund->cr - $fund->dr}}
                @php
                $income += $fund->cr - $fund->dr;
                @endphp
                @else
                -
                @endif
                </td>
                <td class="text-center">
                @if($fund->cr < $fund->dr)
                {{$fund->dr - $fund->cr}}
                @php
                $expense += $fund->dr - $fund->cr;
                @endphp
                @else
                -
                @endif
                </td>
            </tr>
            @endforeach
            <tr>
                <td>Closing Balance</td>
                <td></td>
                <td class="text-center">{{$cbb+$cbc}}</td>
            </tr>
           <tr>
                <td class="text-center"><h5><strong>Total</strong></h5></td>
                <td class="text-center"><strong>{{$obb+$obc+$income}}</strong></td>
                <td class="text-center"><strong>{{$cbc+$cbb+$expense}}</strong></td>
            </tr>
        </table>
        <table class="table">
             <tr>
                <td>Closing Balance Cash</td>
                <td></td>
                <td>{{$cbc}}</td>
            </tr>
            <tr>
                <td>Closing Balance Bank</td>
                <td></td>
                <td>{{$cbb}}</td>
            </tr>
        </table>
        </div>
    </div>
</div>
@endsection