@extends('layout.app')
@section('pageheader','Add New Received')
@section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<form action="{{route('addreceived')}}" role="form" method="POST">
{{ csrf_field() }}
@if(session('message'))
         <div id="myElem" class="alert alert-success">
            {{session('message')}}
         </div>
    @endif
    <div class="col-lg-11" id="payment">
        <div class="row">
            <div class="col-lg-3">
            <label for="">Transection Number</label>
             @if(empty($tra))
                    <input name="tra_num"readonly type="text" class="form-control" value="REC0{{1}}">
                    @else
                    <input name="tra_num" readonly type="text" class="form-control" value="REC0{{$tra+1}}">
                    @endif
            <input type="hidden" name="inv_gen" value="{{$tra+1}}">
            </div>
            <div class="col-lg-2">
              <p>
                <label for="">Date</label>
                <input type="text" readonly name="date" class="form-control"  id="datepicker" placeholder="Click For Select"> 
              </p>
            </div>
        </div> 
        {{-- Row ends here --}}

        {{-- payment start here --}}
        <div>
         <div class="row">
          <table class="table table-responsive">
          <tr>
            <th>Department</th>
            <th>Ledger</th>
            <th>Description</th>  
            <th>Payment</th>
            <th>Received</th>
          </tr>
          <tr v-for="payment in payments">
              <td>
                <select class="form-control" name="dep_id[]">
                <option value="">Please Select</option>
                @foreach($departments as $department)
                    <option value="{{$department->id}}">{{$department->dep_name}}</option>
                @endforeach    
                </select>
              </td>
              <td>
              <select class="form-control" name="led_id[]">
              <option value="">Please Select</option>
                   @foreach($ledgers as $ledger)
                    <option value="{{$ledger->id}}">{{$ledger->led_name}}</option>
                @endforeach 
                </select>
              </td>
              <td><textarea name="description[]"  class="form-control"></textarea></td>
              <td><input type="number" readonly name="dr[]" class="form-control" ></td>
              <td><input type="number" name="cr[]" v-model="payment.cr" class="form-control"></td>
          </tr>
          <tr>
              <td>
                <select class="form-control" name="dep_id[]">
                <option value="">Please Select</option>
                @foreach($departments as $department)
                    <option value="{{$department->id}}">{{$department->dep_name}}</option>
                @endforeach    
                </select>
              </td>
              <td>
              <select class="form-control" name="led_id[]">
              <option value="">Please Select</option>
                   @foreach($bankCash as $bk)
                    <option value="{{$bk->id}}">{{$bk->led_name}}</option>
                @endforeach 
                </select>
              </td>
              <td><textarea name="description[]"  class="form-control"></textarea></td>
              <td><input type="number" v-model="firstDr" name="dr[]" class="form-control" ></td>
              <td><input type="number" readonly name="cr[]"  class="form-control"></td>
          </tr>
          
          </table>
          <p style="display:inline-block;color:green" @click="addline"><i class="fa fa-check"></i> Add new line</p>
        <p style="display:inline-block;color:red;font-size:16px" @click="remove"><i class="fa fa-times"></i> remove new line</p>
        <div class="pull-right">
            <p v-if="sumcr==firstDr" class="btn btn-success">Balanced</p>
            <p v-if="sumcr!=firstDr" class="btn btn-danger">Unblanced</p>
        </div>
         </div>
        </div>
        <br>
        <button v-if="sumcr==firstDr" type="submit" class="btn btn-large btn-primary">Post</button>
    </div>
</form>


@endsection
@section('javascript')
{{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>

//vue js 
var payment =  new Vue({
    el:'#payment',
    data: {
      firstDr:0,
      cr:0,
      payments: [{cr:0}]
    },
    methods: {
     addline: function() {
      this.payments.push({ cr:0});
     },

    remove: function() {
      this.payments.pop();
    }
  },
  computed: {
    sumcr: function () {
        
        var temp=0;
        for(var i=0; i<this.payments.length; i++) {
            temp+= parseInt(this.payments[i].cr);
        };
       return temp; 
    },
  }
});
   $( function() {
    $( "#datepicker" ).datepicker({
        changeMonth: true,
      changeYear: true,
      minDate: -7,
      maxDate:0,
      dateFormat: 'yy-mm-dd',
    }).datepicker().datepicker("setDate", new Date());
    $("#myElem").show().delay(3000).fadeOut();
  } );
</script>

@endsection