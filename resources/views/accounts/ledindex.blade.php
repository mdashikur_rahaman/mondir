@extends('layout.app')
@section('pageheader','All Ledger List')
@section('content')

<table class="table table-responsive table-striped">
    <thead>
        <tr>
        <th>Ledger Name</th>
        <th>Openning Balance</th>
        <th>Ledger Type</th>
        <th>Edit</th>
    </tr>
    </thead>
    <tbody>
        @foreach($ledgers as $ledger)
        <tr>
            <td style="font-size:1.4em">{{$ledger->led_name}}</td>
            <td style="font-size:1.2em">{{$ledger->op_bal}}</td>
            <td style="font-size:1.2em">{{$ledger->led_type}}</td>
            <td>
                <p class="btn btn-danger">Edit</p>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection