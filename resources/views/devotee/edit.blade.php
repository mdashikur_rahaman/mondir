@extends('app')
@section('content')
  <div class="col-lg-12">
                          <div class="card">
                              <div class="card-header"><h5 class="card-header-text">Textual Inputs</h5>

                              </div>

                              <!-- end of modal -->
                              <div class="card-block">
                                  <form>
                                    <div class="row">
                                      <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-6 col-form-label form-control-label">Devotee ID</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" value="{{$devotee->devotee_id}}" id="example-text-input">
                                            </div>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-6 col-form-label form-control-label">Full Name</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" value="{{$devotee->devotee_name}}" id="example-text-input">
                                            </div>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-6 col-form-label form-control-label">Father Name</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" value="{{$devotee->devotee_father_name}}" id="example-text-input">
                                            </div>
                                        </div>
                                      </div>

                                    </div>
                                      <!-- end of First row -->

                                      <div class="row">
                                        <div class="col-md-4">
                                          <div class="form-group row">
                                              <label for="example-text-input" class="col-xs-6 col-form-label form-control-label">Mother Name</label>
                                              <div class="col-sm-10">
                                                  <input class="form-control" type="text" value="{{$devotee->devotee_mother_name}}" id="example-text-input">
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group row">
                                            <label for="exampleSelect1" class="col-xs-6 col-form-label form-control-label">Select Gender</label>
                                            <div class="col-sm-10">
                                              <select class="form-control  " id="exampleSelect1">
                                                  <option>Male</option>
                                                  <option>Female</option>
                                              </select>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group row">
                                              <label for="example-email-input" class="col-xs-6 col-form-label form-control-label">Current Location</label>
                                              <div class="col-sm-10">
                                                  <input class="form-control" type="text" value="{{$devotee->devotee_location}}" id="example-email-input">
                                              </div>
                                          </div>
                                        </div>
                                        {{-- <div class="col-md-4">
                                          <div class="form-group row">
                                              <label for="example-tel-input" class="col-xs-6 col-form-label form-control-label">Telephone</label>
                                              <div class="col-sm-10">
                                                  <input class="form-control" type="tel" value="1-(555)-555-5555" id="example-tel-input">
                                              </div>
                                          </div>
                                        </div> --}}
                                      </div>

                                        <!-- end of Second row -->
                                        <div class="row">
                                          <div class="col-md-4">
                                            <div class="form-group row">
                                              <label for="exampleSelect1" class="col-xs-6 col-form-label form-control-label">Devotee category</label>
                                              <div class="col-sm-10">
                                                <select class="form-control  " id="exampleSelect1">
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                              </div>
                                          </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group row">
                                              <label for="exampleSelect1" class="col-xs-6 col-form-label form-control-label">Select Gender</label>
                                              <div class="col-sm-10">
                                                <select class="form-control  " id="exampleSelect1">
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                              </div>
                                          </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group row">
                                              <label for="exampleSelect1" class="col-xs-6 col-form-label form-control-label">Select Gender</label>
                                              <div class="col-sm-10">
                                                <select class="form-control  " id="exampleSelect1">
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                              </div>
                                          </div>
                                          </div>
                                        </div>




                                      <div class="form-group row">
                                          <label for="example-number-input" class="col-xs-2 col-form-label form-control-label">Number</label>
                                          <div class="col-sm-10">
                                              <input class="form-control" type="number" value="42" id="example-number-input">
                                          </div>
                                      </div>

                                      <div class="form-group row">
                                          <label for="example-date-input" class="col-xs-2 col-form-label form-control-label">Date</label>
                                          <div class="col-sm-10">
                                              <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                                          </div>
                                      </div>


                                      <div class="row">
                                        <div class="col-md-4">
                                          <div class="form-group row">
                                              <label for="file" class="col-md-6 col-form-label form-control-label">Devotee Image</label>
                                              <div class="col-md-8">
                                                  <label for="file" class="custom-file">
                                                      <input type="file" id="file" class="custom-file-input">
                                                      <span class="custom-file-control"></span>
                                                  </label>
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group row">
                                              <label for="file" class="col-md-6 col-form-label form-control-label">NOC Scan Copy</label>
                                              <div class="col-md-8">
                                                  <label for="file" class="custom-file">
                                                      <input type="file" id="file" class="custom-file-input">
                                                      <span class="custom-file-control"></span>
                                                  </label>
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group row">
                                              <label for="file" class="col-md-6 col-form-label form-control-label">NID Scan Copy</label>
                                              <div class="col-md-8">
                                                  <label for="file" class="custom-file">
                                                      <input type="file" id="file" class="custom-file-input">
                                                      <span class="custom-file-control"></span>
                                                  </label>
                                              </div>
                                          </div>
                                        </div>

                                      </div>



                                  </form>
                              </div>
                          </div>
                      </div>
@endsection
