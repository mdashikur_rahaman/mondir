@extends('layout.app')
@section('pageheader')
 Devotee List
@endsection
@section('stylesheet')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<style>
td {
  font-size:16px;
  text-transform: capitalize;
}
</style>
@endsection
@section('content')
  <div class="row">
      <div class="col-sm-12">
      
           
            <h3 class="pull-right"><a href="{{route('devotee.create')}}">Add New Devotee</a></h3>
         
            <table id="example" class="table dt-responsive table-striped table-bordered nowrap">
              <thead>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Name</th>
                <th>Father Name</th>
                <th> Age</th>
                <th>Birth date</th>
                <th>Type</th>
                <th>Details</th>
              </tr>
              </thead>
              <tfoot>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Name</th>
                <th>Father Name</th>
                <th>Age</th>
                <th>Birth date</th>
                <th>Type</th>
                <th>Details</th>
              </tr>
              </tfoot>
              <tbody>
              @foreach($devotee as $item)
              <tr role="row" class="odd">
                <td>{{$item->devotee_id}}</td>
                <td><img src="{{asset('userimages/'.$item->picture)}}" height="42" width="42"></td>
                <td>{{$item->devotee_name}}</td>
                <td>{{$item->devotee_father_name}}</td>
                <td>{{$item->age}}</td>
                <td>{{$item->date_of_birth}}</td>
                <td>{{$item->devotee_category}}</td>
                <td><button type="button" class="btn btn-success"><a href="{{route('devotee.show',$item->id)}}">Details</a></button></td>
              </tr>
              @endforeach

              </tbody>
            </table>

      </div>
    </div>
@endsection
@section('javascript')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection