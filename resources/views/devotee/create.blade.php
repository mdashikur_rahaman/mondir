@extends('layout.app') @section('pageheader','Add New Devotee') @section('stylesheet')
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet"> @endsection @section('content') @if (count($errors) > 0)
<div class="card-block button-list notifications">
    <ul>
        @foreach ($errors->all() as $error)
        <li class="btn btn-danger waves-effect" data-type="inverse" data-animation-in="animated fadeIn" data-animation-out="animated fadeOut">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif {!! Form::open(['route'=>'devotee.store','files'=>true]) !!}
<div class="row">
    <div class="col-lg-2">
        <label>Devotee ID</label>
        <input type="text" class="form-control" name="devotee_id" id="did">

    </div>
    <div class="col-lg-3">

        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name">

        </div>
    </div>
    <div class="col-lg-3">

        <div class="form-group">
            <label>Devotee Name</label>
            <input type="text" class="form-control" name="devotee_name">

        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <label>Current Location</label>
            <input type="text" class="form-control" name="devotee_location">
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <label>Devotee Category</label>
            <select class="form-control" name="devotee_category">
                                            <option>Select Category</option>
                                            <option value="Sannyas">Sannyas</option>
                                            <option value="Brammochary">Brammochary</option>
                                            <option value="Dikkhito">Dikkhito</option>
                                            <option value="Undikkhito">Undikkhito</option>
                                        </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <label>Name Of Father</label>
            <input type="text" class="form-control" name="devotee_father_name">
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>Name Of Mother</label>
            <input type="text" class="form-control" name="devotee_mother_name">
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <label>Date Of Birth</label>
            <input type="text" id="datepicker" class="form-control" name="date_of_birth">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <label>Contact Number</label>
            <input type="text" class="form-control" name="contact_number">
        </div>
    </div>
<div class="col-lg-2">
    <div class="form-group">
        <label>Blood Group</label>
        <select class="form-control" name="blood_group">
                                        <option>Blood Group</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                    </select>
    </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label>NID Number</label>
            <input type="text" class="form-control" name="nid">
        </div>
    </div>
    <div class="col-lg-1">
        <div class="form-group">
            <label>Age</label>
            <input type="text" class="form-control" name="age">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <label>Gender...</label>
            <select class="form-control" name="gender">
                                      <option>Gender...</option>
                                      <option value="Male">Male</option>
                                      <option value="Female">Female</option>
                                      <option value="Other">Other</option>
                                  </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label>Nationality</label>
            <input type="text" class="form-control" name="nationality">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label for="">Educational Qualification</label> {!! Form::select('edu', ['Under S.S.C' => 'Under S.S.C', 'S.S.C'
            => 'S.S.C','H.S.C'=>'H.S.C','Bachelor'=>'Bachelor','Masters'=>'Masters','Other'=>'Other'], null, ['placeholder'
            => 'Educational Qualification...','class'=>'form-control'])!!}
        </div>
    </div>
</div>
{{-- upload profile picture --}}
<div class="row">
    <div class="col-lg-3">
        <div class="group-add-on">
            <span class="add-on-file">
                                            <p class="btn btn-success waves-effect waves-light">Profile Picture</p>
                                        </span>
            <div class="input-file">
                {!! Form::file('picture')!!}
                <label class="label-file">Upload Image</label>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="group-add-on">
            <span class="add-on-file">
                                            <p class="btn btn-success waves-effect waves-light">NOC Scan Copy</p>
                                        </span>
            <div class="input-file">
                <input type="file" class="" name="noc_img">
                <label class="label-file">Upload Image</label>

            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="group-add-on">
            <span class="add-on-file">
                                            <p class="btn btn-success waves-effect waves-light">NID Scan Copy</p>
                                        </span>
            <div class="input-file">
                <input type="file" class="" name="nid_img">
                <label class="label-file">Upload Image</label>

            </div>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-lg-2 col-lg-offset-4">
                <input type="Submit" class="btn btn-primary btn-lg btn-block" value="save" style="margin-top:20px">
            </div>
        </div>

    </div>

    {!! Form::close() !!}
</div>
</div>
</div>

@endsection @section('javascript')
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $(function () {
            $("#datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:2017",
                dateFormat: 'dd-mm-yy',
            });
        });
    });
</script>
@endsection