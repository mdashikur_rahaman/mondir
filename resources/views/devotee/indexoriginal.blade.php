@extends('app')
@section('page_name')
  Add New Devotee
@endsection
@section('content')
  <div class="col-md-8 col-md-offset-2">
      <button class="btn btn-success btn-md">
          <a href="{{ route('devotee.create') }}">Add New Department</a>
      </button>
		<div class="text-center">
    <table class="table table-bordered" id="table">
    <hr>
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">Name</th>
                <th class="text-center">Phone Number</th>
                <th class="text-center">photo</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        @foreach($devotee as $item)
        <tr class="">
            <td>{{$item->id}}</td>
            <td>{{$item->devotee_name}}</td>
            <td>{{$item->contact_number}}</td>
            <td><img src="{{Storage::url('app/'.$item->picture)}}" alt="yty"></td>
            <td><button class="edit-modal btn btn-info" data-id="{{$item->id}}"
                    data-name="{{$item->devotee_name}}">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
                <button class="delete-modal btn btn-danger" data-id="{{$item->id}}"
                    data-name="{{$item->devotee_name}}">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button></td>
        </tr>
        @endforeach
    </table>
</div>
@endsection
