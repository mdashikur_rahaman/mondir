@extends('layout.app') @section('pageheader','Devotee Information')
@section('stylesheet')

<style>
td {
  font-size:16px;
  text-transform: capitalize;
}
</style>
@endsection
 @section('content')

<div class="row">
    <div class="col-sm-10">

        <button class="btn btn-warning pull-right">Update Info</button>

            <div id="view-info" class="row">
                <div class="col-md-4 col-lg-3">

                    
                        <h3 class="text-center">ID <span class="text-success">{{$devotee->devotee_id}}<span></h3>
                            <img class="img-responsive" src="{{asset('userimages/'.$devotee->picture)}}" alt="">
                        </div>
                          <h3 class="text-center text-primary">{{$devotee->devotee_category}}</h3>
                   
                    <div class="col-lg-6 col-md-6">
                        <form>
                            <table class="table">
                                <tr>
                                    <th>Full Name</th>
                                    <td >{{$devotee->devotee_name}}</td>
                                </tr>
                                <tr>
                                    <th >Father Name</th>
                                    <td >{{$devotee->devotee_father_name}}</td>
                                </tr>
                                <tr>
                                    <th >Mother Name</th>
                                    <td>{{$devotee->devotee_mother_name}}</td>
                                </tr>
                                <tr>
                                    <th >Gender</th>
                                    <td >{{$devotee->gender}}</td>
                                </tr>
                                <tr>
                                    <th >Birth Date</th>
                                    <td >{{$devotee->date_of_birth}}</td>
                                </tr>
                                <tr>
                                  <th>Location</th>
                                  <td>{{$devotee->devotee_location}}</td>
                                </tr>
                                <tr>
                                  <th >Mobile Number</th>
                                  <td>{{$devotee->contact_number}}</td>
                                </tr>
                                <tr>
                                  <th >Blood Group</th>
                                  <td >{{$devotee->blood_group}}</td>
                                </tr>
                                <tr>
                                  <th >NID Number</th>
                                  <td >{{$devotee->nid}}</td>
                                </tr>
                              </table>
                          </form>
        

    
</div>

@endsection