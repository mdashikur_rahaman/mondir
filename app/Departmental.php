<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departmental extends Model
{
    protected $table = 'depinfos';
}
