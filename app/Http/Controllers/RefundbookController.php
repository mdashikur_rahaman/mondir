<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Validator;
use App\Refundbook;
class RefundbookController extends Controller
{
    public function rb() {
        $customers = DB::select('SELECT id,temple_name from customers');
        $books = DB::select('select id,book_name from books');
        return view('stock.books.refundbook',['books'=>$books,'customers'=>$customers]);
   }
   public function arb(Request $request) {
     //validate the data
          $validator = Validator::make($request->all(), [
            'refund_date' => 'required',
            'inv_num' => 'required',
            'customer_id' => 'required',
            'book_id' => 'required|numeric',
            'pcs' => 'required|numeric',
            'amount' => 'required|numeric',
        ]);
         if ($validator->fails()) {
            return Redirect::to(route('rb'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $book = new Refundbook;
        $book->refund_date = $request->refund_date;
        $book->inv_num=$request->inv_num;
        $book->pcs=$request->pcs;
        $book->customer_id=$request->customer_id;
        $book->item_id=$request->book_id;
        $book->item_type='book';
        $book->amount=$request->amount;
        $book->save();
        return Redirect::to(route('rb'));
   }
}
