<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Stock;
use Validator;
use App\BookPurchase;
class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer()
    {
        return view('stock.addcustomer');
    }
     public function addbook()
    {
        return view('stock.books.addbook');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function purchasebook()
    {   
        $books = DB::select('select id,book_name from books ORDER BY book_name ASC');
        return view('stock.books.purchasebook',['books'=>$books]);
    }
    public function addpurchasebook(Request $request)
    {   $validator = Validator::make($request->all(), [
            'book_id' => 'required',
            'pcs' => 'required|numeric',
            'purchases_price' => 'required',
            'sales_price' => 'required|numeric',
            'date' => 'required|date',
            'challan_no' => 'required|numeric',
        ],['book_id.required'=>'Please Selecet A Book From List']);
         if ($validator->fails()) {
            return Redirect::to(route('purchasebook'))
                        ->withErrors($validator)
                        ->withInput();
        }
         $pbook = new BookPurchase;
         $pbook->item_id = $request->book_id;
         $pbook->pcs = $request->pcs; 
         $pbook->item_type = 'book';
         $pbook->purchases_price = $request->purchases_price; 
         $pbook->sales_price = $request->sales_price; 
         $pbook->date = $request->date; 
         $pbook->challan_no = $request->challan_no; 

         $pbook->save();
         return Redirect::to(route('salebook'))->with('message','Sales information Successfully saved');
    }

  
    public function addcustomer(Request $request)
    {   
       //validator
       $validator = Validator::make($request->all(), [
            'temple_name' => 'required|unique:customers'
        ],['temple_name.unique'=>'Customer Already Exists!']);
         if ($validator->fails()) {
            return Redirect::to(route('customer'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $customer = new Stock;
        $customer->temple_name = $request->temple_name;
        $customer->address = $request->address;
        $customer->phone = $request->phone;
        $customer->contact_person = $request->contact_person;
        $customer->openning_bal = $request->openning_bal;
        $customer->save();
        return Redirect::to(route('customer'))->with('message','Customer Information succesfully Saved');
    }
    //sales book 
     public function salebook()
    {   
        $books = DB::select('select id,book_name from books');
        $customers = DB::select('select id,temple_name from customers');
        $price = DB::select('select id,temple_name from customers');
        return view('stock.purchasebook',['books'=>$books]);
    }

}
