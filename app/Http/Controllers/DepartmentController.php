<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Department;
use Validator;
class DepartmentController extends Controller
{   
    public function index() {
        $departments = Department::orderBy('created_at', 'desc')
            ->paginate(8);
        return view('accounts.depindex',compact('departments'));
    }
    public function dep()
    {
        return view('accounts.addnewdep');
    }
    public function adddep(Request $request)
    {   
        //validate the data
          $validator = Validator::make($request->all(), [
            'dep_name' => 'required|unique:departments'
        ]);
         if ($validator->fails()) {
            return Redirect::to(route('dep'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $dept = new Department;
        $dept->dep_name = $request->dep_name;
        $dept->op_bal_cash=$request->op_bal_cash;
        $dept->op_bal_bank=$request->op_bal_bank;
        $dept->save();
        return Redirect::to(route('dep'))->with('status','Department Information Successfully Save');
    }
}
