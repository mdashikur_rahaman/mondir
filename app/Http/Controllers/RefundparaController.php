<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Validator;
use App\Refundpara;
class RefundparaController extends Controller
{
    public function rp() {
        $customers = DB::select('SELECT id,temple_name from customers');
        $paras = DB::select('select id,para_name from paras');
        return view('stock.paras.refundpara',['paras'=>$paras,'customers'=>$customers]);
   }
   public function arp(Request $request) {
     //validate the data
          $validator = Validator::make($request->all(), [
            'refund_date' => 'required',
            'inv_num' => 'required',
            'customer_id' => 'required',
            'para_id' => 'required|numeric',
            'pcs' => 'required|numeric',
            'amount' => 'required|numeric',
        ]);
         if ($validator->fails()) {
            return Redirect::to(route('rp'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $book = new Refundpara;
        $book->refund_date = $request->refund_date;
        $book->inv_num=$request->inv_num;
        $book->pcs=$request->pcs;
        $book->customer_id=$request->customer_id;
        $book->item_id=$request->para_id;
        $book->item_type='para';
        $book->amount=$request->amount;
        $book->save();
        return Redirect::to(route('rp'));
   }
}
