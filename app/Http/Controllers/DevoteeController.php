<?php

namespace App\Http\Controllers;

use App\Devotee;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Storage;
use Session;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class DevoteeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $devotee = Devotee::all();
      return view('devotee.index')->with('devotee',$devotee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('devotee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $validator = Validator::make($request->all(),[
        'devotee_id'=> 'required',
        'devotee_name'=> 'required',
        'devotee_location'=> 'required',
        'devotee_category'=> 'required',
        'devotee_father_name'=> 'required',
        'devotee_mother_name'=> 'required',
        'date_of_birth'=> 'required',
        'nationality'=> 'required',
        'contact_number'=> 'required|numeric',
        'blood_group'=> 'required',
        'nid'=>'required',
        'age'=> 'required|numeric',
        'gender'=> 'required',
        'edu'=> 'required',

      ]
    );
      if ($validator->fails()) {
            return Redirect::to('devotee/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $devotee = new Devotee;
            $devotee->devotee_id = $request->devotee_id;
            $devotee->name = $request->name;
            $devotee->devotee_name = $request->devotee_name;
            $devotee->devotee_location = $request->devotee_location;
            $devotee->devotee_category = $request->devotee_category;
            $devotee->devotee_father_name = $request->devotee_father_name;
            $devotee->devotee_mother_name = $request->devotee_mother_name;
            $devotee->date_of_birth = $request->date_of_birth;
            $devotee->nationality = $request->nationality;
            $devotee->contact_number = $request->contact_number;
            $devotee->blood_group = $request->blood_group;
            $devotee->age = $request->age;
            $devotee->nid = $request->nid;
            $devotee->gender = $request->gender;
            $devotee->edu = $request->edu;
            //save to db
            if ($request->hasFile('picture')) {
              $image = $request->file('picture');
              $filename= 'pic'.time().'.'.$image->getClientOriginalExtension();
              $location = public_path('userimages/'. $filename);
              Image::make($image)->resize(200,200)->save($location);
              $devotee->picture = $filename;
            }
            if ($request->hasFile('noc_img')) {
              $image = $request->file('noc_img');
              $filename= 'noc'.time().'.'.$image->getClientOriginalExtension();
              $location = public_path('userimages/'. $filename);
              Image::make($image)->resize(200,200)->save($location);
              $devotee->noc_img = $filename;
            }
            if ($request->hasFile('nid_img')) {
              $image = $request->file('nid_img');
              $filename= 'nid'.time().'.'.$image->getClientOriginalExtension();
              $location = public_path('userimages/'. $filename);
              Image::make($image)->resize(200,200)->save($location);
              $devotee->nid_img = $filename;
            }

            $devotee->save();

            // redirect
            Session::flash('message', 'Successfully created nerd!');
            return Redirect::to('devotee');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Devotee  $devotee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $devotee= Devotee::find($id);
        return view('devotee.show')->withDevotee($devotee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Devotee  $devotee
     * @return \Illuminate\Http\Response
     */
    public function edit(Devotee $devotee)
    {
      $devotee= Devotee::find(8);
      return view('devotee.edit')->withDevotee($devotee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Devotee  $devotee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Devotee $devotee)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Devotee  $devotee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Devotee $devotee)
    {
        //
    }
}
