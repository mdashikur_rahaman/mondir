<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class ParareportController extends Controller {
    public function inq() {
        return view('stock.paras.reports.allparaq');
    }
    public function all (Request $request) {
    $fm = $request->fmonth;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
    $paras = DB::select("
  SELECT 
  paras.id,
  paras.para_name,
  paras.opn_pcs AS openning,
  SUM(P.pcs) AS purchase,
  IFNULL(SUM(S.sales),0) AS sales,
  IFNULL(SUM(R.refund),0) AS refund,
  IFNULL(SUM(T.wasted),0) AS wasted,
  SUM(P.purchase) AS purchase_price 
FROM
  paras
  LEFT JOIN 
    (SELECT 
      purchaseitems.`pcs` AS pcs,
      purchaseitems.`purchases_price` AS purchase,
      purchaseitems.`item_id` 
    FROM
      purchaseitems 
    WHERE purchaseitems.`item_type` = 'para' AND `purchaseitems`.`date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31')) AS P 
    ON P.`item_id` = paras.`id` 
  LEFT JOIN 
    (SELECT 
      stocksales.`pcs` AS sales,
      stocksales.`item_id` 
    FROM
      stocksales 
    WHERE stocksales.`item_type` = 'para' AND `stocksales`.`date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31')) AS S 
    ON S.`item_id` = paras.`id` 
  LEFT JOIN 
    (SELECT 
      refunditems.`pcs` AS refund,
      refunditems.`item_id` 
    FROM
      refunditems 
    WHERE refunditems.`item_type` = 'para' AND `refunditems`.`refund_date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31') ) AS R 
    ON R.`item_id` = paras.`id` 
  LEFT JOIN 
    (SELECT 
      transferitems.`pcs` AS wasted,
      transferitems.`item_id` 
    FROM
      transferitems 
    WHERE transferitems.`item_type` = 'para') AS T 
    ON T.`item_id` = paras.`id` 
GROUP BY paras.`id` 
    ");
        return view('stock.paras.reports.allparabalance',['paras'=>$paras]);
    }
    
}