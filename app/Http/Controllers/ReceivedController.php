<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DB;
class ReceivedController extends Controller
{
    public function received() {
        $tra =DB::table('receiveds')->max('inv_gen');
        $departments = DB::select('SELECT id,dep_name from departments ORDER BY  dep_name ASC');
         $ledgers = DB::select("SELECT id,led_name from ledgers WHERE led_type = 'income' ORDER BY led_name ASC");
         $bankCash = DB::table('ledgers')
         ->where('led_type','cash')
         ->orWhere('led_type','bank')
         ->orderBy('led_name', 'asc')->get();
        return view('accounts.receiveds',['departments'=>$departments,'ledgers'=>$ledgers,'tra'=>$tra,'bankCash'=>$bankCash]);
    }

    public function addreceived(Request $request) {
        $transectionsdata = $request->all();
        $num_elements = 0;
        $transections = array();
        while($num_elements < count($transectionsdata['dep_id'])){
            $transections[] = array(
                't_type'=>'r',
                'dep_id' => $transectionsdata['dep_id'][$num_elements],
                'led_id'   => $transectionsdata['led_id'][$num_elements],
                'description' => $transectionsdata['description'][$num_elements],
                'dr'  => $transectionsdata['dr'][$num_elements],
                'cr'  => $transectionsdata['cr'][$num_elements],
                'date' => $transectionsdata['date'],
                'tra_num' => $transectionsdata['tra_num']
                
            );
            $recinv[] = ['inv_gen'=>$transectionsdata['inv_gen']];
            $num_elements++;
        }
        DB::table('transections')->insert($transections);
        DB::table('receiveds')->insert($recinv);
        return Redirect::to(route('received'))->with('message','Received Successfully Posted');
    }
}
