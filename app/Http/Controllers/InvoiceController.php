<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function book() {
        $max = DB::select("SELECT MAX(id) AS id FROM `stockinvoices`
WHERE item_type = 'book'");
        $info = DB::table('stockinvoices')->select('inv_num')
        ->where('id','=',$max[0]->id)
        ->get();
        $finfo = $info[0]->inv_num;
        $infos = DB::select("SELECT date,inv_num,customer_id FROM `stocksales` WHERE inv_num = '$finfo' ");
        $cid= $infos[0]->customer_id;
        $customer = DB::select("SELECT temple_name,address FROM customers WHERE id = $cid");
        $items = DB::select("
        SELECT books.`book_name` AS book,stocksales.pcs AS pcs,price,purchaseitems.`sales_price` AS sales FROM stocksales
JOIN books
ON books.id = stocksales.`item_id`
JOIN purchaseitems
ON stocksales.`item_id` = purchaseitems.`item_id`
 WHERE purchaseitems.`item_type`='book' AND stocksales.`inv_num`='$finfo'
        ");
        $total = DB::select("SELECT total_amount AS total,rcv_amount AS rcv,discount,due FROM stockinvoices
 WHERE stockinvoices.`item_type`='book' AND stockinvoices.`inv_num`='$finfo '");
       return view('stock.bookinvoice',['customer'=>$customer[0]->temple_name,'address'=>$customer[0]->address,'infos'=>$infos,'items'=>$items,'total'=>$total[0]]);

      
    }


     public function para() {
        $max = DB::select("SELECT MAX(id) AS id FROM `stockinvoices`
WHERE item_type = 'para'");
        $info = DB::table('stockinvoices')->select('inv_num')
        ->where('id','=',$max[0]->id)
        ->get();
        $finfo = $info[0]->inv_num;
        $infos = DB::select("SELECT date,inv_num,customer_id FROM `stocksales` WHERE inv_num = '$finfo' ");
        $cid= $infos[0]->customer_id;
        $customer = DB::select("SELECT temple_name,address FROM customers WHERE id = $cid");
        $items = DB::select("
        SELECT paras.`para_name` AS book,stocksales.pcs AS pcs,price,purchaseitems.`sales_price` AS sales FROM stocksales
JOIN paras
ON paras.id = stocksales.`item_id`
JOIN purchaseitems
ON stocksales.`item_id` = purchaseitems.`item_id`
 WHERE purchaseitems.`item_type`='para' AND stocksales.`inv_num`='$finfo'
        ");
        $total = DB::select("SELECT total_amount AS total,rcv_amount AS rcv,discount,due FROM stockinvoices
 WHERE stockinvoices.`item_type`='para' AND stockinvoices.`inv_num`='$finfo '");
       return view('stock.bookinvoice',['customer'=>$customer[0]->temple_name,'address'=>$customer[0]->address,'infos'=>$infos,'items'=>$items,'total'=>$total[0]]);

      
    }
}
