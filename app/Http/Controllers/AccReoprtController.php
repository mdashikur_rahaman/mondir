<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class AccReoprtController extends Controller
{
    public function index() {
        return view('accounts.reports.dateselect');
    }
   

    public function alldipreport(Request $request) {
        $fm = $request->fmonth;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
        $obb =DB::table('departments')
        ->sum('op_bal_bank');
        $obc =DB::table('departments')
        ->sum('op_bal_cash');
        //total op bal
        $tob = $obb + $obc;
        //openning balance cash
        $opbalcash = DB::select("SELECT SUM(transections.dr) AS dr,SUM(transections.cr) AS cr
            FROM transections
            JOIN ledgers
            ON transections.`led_id`=ledgers.`id`
            WHERE ledgers.`led_type` = 'cash'  AND MONTH(transections.`date`)<$fm AND YEAR(transections.`date`)=$fy");
        $obcf = $obc + $opbalcash[0]->dr - $opbalcash[0]->cr;
        //openning balance 
        $drcro = DB::select("SELECT SUM(dr) AS dr,SUM(cr) AS cr FROM `transections` AS T JOIN `ledgers` AS L ON L.`id`=T.`led_id` WHERE L.`led_type`!='cash' AND L.`led_type`!='bank' AND MONTH(T.`date`)<$fm AND YEAR(T.`date`)=$fy");
        $openning = $tob + $drcro[0]->cr - $drcro[0]->dr;
        //closing 
        $drcrc = DB::select("SELECT SUM(dr) AS dr,SUM(cr) AS cr FROM `transections` AS T JOIN `ledgers` AS L ON L.`id`=T.`led_id` WHERE L.`led_type`!='cash' AND L.`led_type`!='bank' AND MONTH(T.`date`)<$tm+1 AND YEAR(T.`date`)=$ty");
        $closing = $obb+$obc+ $drcrc[0]->cr - $drcrc[0]->dr;
        $total =$obb+$obc+ $drcrc[0]->cr ;
        // all dep rcv pay bal
        $alldep = DB::select("SELECT dep_name,SUM(transections.dr) AS dr,SUM(transections.cr) AS cr
FROM transections
JOIN departments
ON `transections`.`dep_id`=departments.`id`
JOIN ledgers
ON transections.`led_id`=ledgers.`id`
WHERE ledgers.`led_type` != 'cash' AND  ledgers.`led_type` != 'bank' AND transections.`date` BETWEEN '$fy-$fm-01' AND '$ty-$tm-31'
GROUP BY transections.dep_id"
        );
 
    return view('accounts.reports.alldep',['fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty,'obb'=>$obb,'obc'=>$obcf,'openning'=>$openning,'alldep'=>$alldep,'closing'=>$closing,'total'=>$total]);
    }
    // department wise report
    public function dipwise() {
        $deps=DB::select("SELECT id,dep_name FROM departments GROUP BY dep_name ASC");
        return view('accounts.reports.depwiseq',['deps'=>$deps]);
    }
     public function addepwise(Request $request) {
        $fm = $request->fmonth;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
         $dep = $request->dep;
         $depName= DB::select("SELECT `dep_name` FROM departments AS D WHERE D.`id`= $dep");
        $obb  = DB::select("SELECT `op_bal_bank` AS opb FROM departments AS D WHERE D.`id`= $dep");
        $obc  = DB::select("SELECT `op_bal_cash` AS opc FROM departments AS D WHERE D.`id`= $dep");
        // First starting balance
        $fob= $obb[0]->opb +$obc[0]->opc;
        // cr dr form database
        $ocrdr = DB::select("SELECT SUM(dr) AS dr,SUM(cr) AS cr 
FROM `transections` AS T JOIN `ledgers` AS L ON L.`id`=T.`led_id`
 WHERE L.`led_type`!='cash' AND L.`led_type`!='bank' AND MONTH(T.`date`)<$fm AND YEAR(T.`date`)=$fy AND T.`dep_id`=$dep");
  $closingdb = DB::select("SELECT SUM(dr) AS dr,SUM(cr) AS cr 
FROM `transections` AS T JOIN `ledgers` AS L ON L.`id`=T.`led_id`
 WHERE L.`led_type`!='cash' AND L.`led_type`!='bank' AND MONTH(T.`date`)<$tm+1 AND YEAR(T.`date`)=$ty AND T.`dep_id`=$dep");
 // final op bal 
 $finalopbal= $fob + $ocrdr[0]->cr -$ocrdr[0]->dr;
 //final closing balance
  $finalclosing= $fob + $closingdb[0]->cr -$closingdb[0]->dr;
  $total = $finalclosing + $closingdb[0]->dr;
 //ledger information
        $ledinfos = DB::select("SELECT L.`led_name`  AS ledname, IFNULL(dr,0) AS dr,
IFNULL(cr,0) AS cr
 FROM transections AS T
 JOIN ledgers AS L
 ON T.`led_id`=L.`id`

WHERE dep_id=$dep AND L.`led_type`!= 'cash' AND L.`led_type`!='bank' AND T.`date` BETWEEN '$fy-$fm-01'AND '$ty-$tm-31'");
       

       return view('accounts.reports.depwise',['dep'=>$depName[0]->dep_name,'openning'=>$fob,'fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty,'ledinfos'=>$ledinfos,'fopb'=>$finalopbal,'finalclosing'=>$finalclosing,'total'=>$total]);
    }
public function fund() {
    return view('accounts.reports.fund');
}
public function fundstate(Request $request) {
        $fm = $request->fmonth;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
        $initialopbalbank = DB::select("SELECT SUM(op_bal) AS opb FROM ledgers WHERE led_type='bank'");
        $initialopbalcash = DB::select("SELECT SUM(op_bal) AS opc FROM ledgers WHERE led_type='cash'");
        $opbalbank = DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='bank' AND transections.date <'$fy-$fm-01'");
        $opbalcash= DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='cash' AND transections.date < '$fy-$fm-01'");
    $totalopbalbank = $initialopbalbank[0]->opb +$opbalbank[0]->dr-$opbalbank[0]->cr;
    $totalopbalcash = $initialopbalcash[0]->opc +$opbalcash[0]->dr-$opbalcash[0]->cr;
    // department wise fund statement
    $fundstatement = DB::select("SELECT departments.`dep_name` AS dep,SUM(dr) AS dr,SUM(cr) AS cr FROM transections
JOIN ledgers
ON transections.`led_id` = ledgers.`id`
JOIN departments
ON transections.`dep_id` = departments.`id`
WHERE ledgers.`led_type` !='bank' AND ledgers.`led_type` != 'cash' AND transections.`date` BETWEEN '$fy-$fm-01' AND '$ty-$tm-31'
GROUP BY departments.`dep_name` ASC");
  // closing balance 
  $clbalbank = DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='bank' AND transections.date <='$ty-$tm-31'");

$clbalcash = DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='cash' AND transections.date <='$ty-$tm-31'");

$totalclbalbank = $initialopbalbank[0]->opb +$clbalbank[0]->dr-$clbalbank[0]->cr;
$totalclbalcash = $initialopbalcash[0]->opc +$clbalcash[0]->dr-$clbalcash[0]->cr;

    return view('accounts.reports.fundstate',['obb'=>$totalopbalbank,'obc'=>$totalopbalcash,'funds'=>$fundstatement,'cbc'=>$totalclbalcash,'cbb'=>$totalclbalbank,'fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty]);
}
public function cashb() {
    return view('accounts.reports.cashrep');
}
// cashbook
public function cashbook(Request $request) {
        $fm = $request->fmonth;
        $fmc= intval($request->fmonth) - 1;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
        $obc =DB::table('departments')
        ->sum('op_bal_cash');
        $opbalcash = DB::select("SELECT SUM(transections.dr) AS dr,SUM(transections.cr) AS cr
            FROM transections
            JOIN ledgers
            ON transections.`led_id`=ledgers.`id`
            WHERE ledgers.`led_type` = 'cash'  AND transections.`date`<='$fy-$fmc-31'");
        $clbalcash = DB::select("SELECT SUM(transections.dr) AS dr,SUM(transections.cr) AS cr
            FROM transections
            JOIN ledgers
            ON transections.`led_id`=ledgers.`id`
            WHERE ledgers.`led_type` = 'cash'  AND transections.`date`<='$ty-$tm-31'");
        $tobc= $obc + $opbalcash[0]->dr - $opbalcash[0]->cr; 
        $tclbc= $obc + $clbalcash[0]->dr - $clbalcash[0]->cr;     
        $cbooks = DB::select("SELECT DATE AS t_date,tra_num,departments.`dep_name` AS dep,ledgers.`led_name` AS led,description AS d,IFNULL(dr,0) AS dr, IFNULL(cr,0) AS cr 
FROM transections
JOIN 
departments
ON
departments.`id`=transections.`dep_id`
JOIN 
ledgers
ON	
ledgers.`id`=transections.`led_id`
WHERE DATE BETWEEN '$fy-$fm-01' AND '$ty-$tm-31' AND ledgers.`led_type`='cash'");
    return view('accounts.reports.cash',['fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty,'tobc'=>$tobc,'cbooks'=>$cbooks,'closing'=>$tclbc]);
}
public function bankb() {
    return view('accounts.reports.banks');
}
public function bankbook(Request $request) {
         $fm = $request->fmonth;     
        $fmc = intval($request->fmonth)-1;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
        $obb =DB::table('departments')
        ->sum('op_bal_bank');
$opbalbank = DB::select("SELECT SUM(transections.dr) AS dr,SUM(transections.cr) AS cr
            FROM transections
            JOIN ledgers
            ON transections.`led_id`=ledgers.`id`
            WHERE ledgers.`led_type` = 'bank' AND transections.`date`<='$fy-$fmc-31' ");
$clbalbank = DB::select("SELECT SUM(transections.dr) AS dr,SUM(transections.cr) AS cr
            FROM transections
            JOIN ledgers
            ON transections.`led_id`=ledgers.`id`
            WHERE ledgers.`led_type` = 'bank'  AND transections.`date`<='$ty-$tm-31'");
$tobb= $obb + $opbalbank[0]->dr - $opbalbank[0]->cr;
$tcbb= $obb + $clbalbank[0]->dr - $clbalbank[0]->cr;
 $bbooks = DB::select("SELECT DATE AS t_date,tra_num,departments.`dep_name` AS dep,ledgers.`led_name` AS led,description AS d,IFNULL(dr,0) AS dr, IFNULL(cr,0) AS cr 
FROM transections
JOIN 
departments
ON
departments.`id`=transections.`dep_id`
JOIN 
ledgers
ON	
ledgers.`id`=transections.`led_id`
WHERE DATE BETWEEN '$fy-$fm-01' AND '$ty-$tm-31' AND ledgers.`led_type`='bank'"); 
    return view('accounts.reports.bankbook',['fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty,'tobb'=>$tobb,'bbooks'=>$bbooks,'closing'=>$tcbb]);
}
public function ledwise() {
  return view('accounts.reports.ledgerwiseq');
}
public function ledgerwise(Request $request) {
    $fm = $request->fmonth;
    $fy = $request->fyear;
    $tm= $request->tmonth;
    $ty = $request->tyear;
    //openning and cloding balance calculate start here
     $initialopbalbank = DB::select("SELECT SUM(op_bal) AS opb FROM ledgers WHERE led_type='bank'");
        $initialopbalcash = DB::select("SELECT SUM(op_bal) AS opc FROM ledgers WHERE led_type='cash'");
        $opbalbank = DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='bank' AND transections.date <'$fy-$fm-01'");
        $opbalcash= DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='cash' AND transections.date < '$fy-$fm-01'");
    $totalopbalbank = $initialopbalbank[0]->opb +$opbalbank[0]->dr-$opbalbank[0]->cr;
    $totalopbalcash = $initialopbalcash[0]->opc +$opbalcash[0]->dr-$opbalcash[0]->cr;
  // closing balance 
  $clbalbank = DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='bank' AND transections.date <='$ty-$tm-31'");

$clbalcash = DB::select("SELECT IFNULL(SUM(dr),0) AS dr, IFNULL(SUM(cr),0) cr FROM ledgers
JOIN
transections
ON ledgers.`id`=transections.`led_id`
WHERE ledgers.`led_type`='cash' AND transections.date <=DATE('$ty-$tm-31')");

$totalclbalbank = $initialopbalbank[0]->opb +$clbalbank[0]->dr-$clbalbank[0]->cr;
$totalclbalcash = $initialopbalcash[0]->opc +$clbalcash[0]->dr-$clbalcash[0]->cr;
 //openning and closing balance calculation close here
    $leds = DB::select("SELECT led_name,IFNULL(SUM(T.`dr`),0) AS dr,IFNULL(SUM(T.cr),0) AS cr FROM ledgers AS L JOIN transections AS T ON L.`id` = T.`led_id` WHERE L.`led_type` !='bank' AND L.`led_type`!= 'cash' AND T.`date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31') GROUP BY L.id");
    return view('accounts.reports.ledgerwise',['leds'=>$leds,'fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty,'tob'=>$totalopbalbank,'toc'=>$totalopbalcash,'tcb'=>$totalclbalbank,'tcc'=>$totalclbalcash]);
}
}
