<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class TransferbookController extends Controller
{
    public function tb() {
        $customers = DB::select('SELECT id,temple_name from customers ORDER BY temple_name ASC');
        $books = DB::select('SELECT id,book_name from books ORDER BY book_name ASC');
        return view('stock.books.transferbook',['customers'=>$customers,'books'=>$books]);
    }
      public function atb(Request $request) {
         $tbooks = $request->all(); 
         $num_elements = 0;
         $sqlData = array();
        while($num_elements < count($tbooks['book_id'])){
            $sqlData[] = array(
                'item_id'     => $tbooks['book_id'][$num_elements],
                'pcs'     => $tbooks['pcs'][$num_elements],
                'item_type'=>'book'
            );
            $num_elements++;
        }
        DB::table('transferitems')->insert($sqlData);
    }
}
