<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DB;
use App\Departmental;
use App\Deptrans;
class DepartmentalController extends Controller
{
    public function inf() {
        return view('departmental.inf');
    }
    public function addinf(Request $request) {
        $validator = Validator::make($request->all(),[
            'dep_name'=> 'required|unique:depinfos',
            'address'=> 'required',
            'con_person'=> 'required',
            'phone'=>'required'
        ]);
        if ($validator->fails()) {
            return Redirect::to('departmental/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $inf = new Departmental;
            $inf->dep_name = $request->dep_name;
            $inf->address = $request->address;
            $inf->con_person = $request->con_person;
            $inf->phone = $request->phone;
            $inf->save();
            return Redirect::to(route('depcreate'))->with('status','Departmental Information Successfully Save');
        }
    }
    public function trans() {
        $deps = DB::table('depinfos')->select('id','dep_name')->get();
        return view('departmental.transection',['deps'=>$deps]);
    }
     public function addtrans(Request $request) {
        $validator = Validator::make($request->all(),[
            'dep_id'=> 'required',
            't_date'=> 'required|date',
            'rosid_num'=> 'required',
            'folio_num'=>'required',
            'item_type'=>'required',
            'name'=>'required',
            'amount'=>'required'
        ]);
        if ($validator->fails()) {
            return Redirect::to(route('deptrans'))
                ->withErrors($validator)
                ->withInput();
        } else {
            $tra = new Deptrans;
            $tra->dep_id = $request->dep_id;
            $tra->t_date = $request->t_date;
            $tra->rosid_num = $request->rosid_num;
            $tra->folio_num = $request->folio_num;
            $tra->item_type = $request->item_type;
            $tra->name = $request->name;
            $tra->amount = $request->amount;
            $tra->save();
            return Redirect::to(route('deptrans'))->with('status','Departmental Transection Successfully Save');
        }
    }

    public function reportq() {
        return view('departmental.reportq');
    }
    public function rtodb(Request $request) {
        $fm= $request->fmonth;
        $fy= $request->fyear;
        $tm= $request->tmonth;
        $ty= $request->tyear;
        $report = DB::select("SELECT t_date,rosid_num,folio_num,name,item_type,amount FROM deptrans
WHERE  t_date BETWEEN '$fy-$fm-1' AND '$ty-$tm-31'");
        $total = DB::select("SELECT SUM(amount) as samo FROM deptrans
WHERE  t_date BETWEEN '$fy-$fm-1' AND '$ty-$tm-31'");
        $totalbook = DB::select("SELECT SUM(amount) as tb FROM deptrans
WHERE  t_date BETWEEN '$fy-$fm-1' AND '$ty-$tm-31' AND item_type='book'");
        $totalpara = DB::select("SELECT SUM(amount) as tp FROM deptrans
WHERE  t_date BETWEEN '$fy-$fm-1' AND '$ty-$tm-31' AND item_type='para'");
        $totalother = DB::select("SELECT SUM(amount) as totalother FROM deptrans
WHERE  t_date BETWEEN '$fy-$fm-1' AND '$ty-$tm-31' AND item_type='other'");

       return view('departmental.reports',['reports'=>$report,'total'=>$total[0]->samo,'tb'=>$totalbook[0]->tb,'tp'=>$totalpara[0]->tp,'to'=>$totalother[0]->totalother,'fm'=>$fm,'fy'=>$fy,'tm'=>$tm,'ty'=>$ty]);
    }
}
