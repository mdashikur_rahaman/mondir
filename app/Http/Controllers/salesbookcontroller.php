<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
class salesbookcontroller extends Controller
{
    public function salebook(){
        $inv =DB::table('bookinvoices')->max('id');
        $customers = DB::select('SELECT id,temple_name from customers');
        $books = DB::table('books')
        ->join('purchaseitems', 'books.id', '=', 'purchaseitems.item_id')
        ->select('books.id','books.book_name', 'purchaseitems.sales_price')
        ->where('item_type','=','book')
         ->get();
        return view('stock.books.salebooks',['customers'=>$customers,'books'=>$books,'inv'=>$inv]);
    }

     public function addsalebook(Request $request){
    //save every book data
       $invoiceid= $request->inv_gen;
       $saledata = $request->all(); 
       $catchId =  $request->book_id;
       $id=[];
       for($i=0;$i<count($catchId);$i++){
        $temp = explode("|",$catchId[$i]);
        array_push($id,$temp[1]);
       }
        $num_elements = 0;
        $sqlData = array();
        while($num_elements < count($saledata['book_id'])){
            $sqlData[] = array(
                'customer_id'         => $saledata['customer_id'],
                'inv_num'          => $saledata['inv_num'],
                'item_id'     => $id[$num_elements],
                'item_type'=>'book',
                'pcs'     => $saledata['pcs'][$num_elements],
                'price'     => $saledata['price'][$num_elements],
                'date'     => $saledata['date'],
            );
            $num_elements++;
        }
        DB::table('stocksales')->insert($sqlData);
        DB::table('bookinvoices')->insert(['gen'=>$invoiceid]);
       // save invoice data
       $invoice = array (
           'customer_id'         => $saledata['customer_id'],
            'inv_num'          => $saledata['inv_num'],
            'item_type'  =>'book',
            'pay_type'          => $saledata['pay_type'],
            'total_amount'          => $saledata['total_amount'],
            'rcv_amount'          => $saledata['rcv_amount'],
            'due'          => $saledata['total_amount']-$saledata['rcv_amount'],
            'cheque_num'   => $saledata['cheque_num'],
            'discount'     =>$saledata['discount'],
            'date'     => $saledata['date'],
       );
       DB::table('stockinvoices')->insert($invoice);
     return Redirect::to(route('purchasebook'))->withStatus('Sales information Successfully saved');

    }
}
