<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DB;

class CustomerStatusController extends Controller
{
     public function stat(Request $request) {
        return view('stock.customer.stat');
    }
    public function status(Request $request) {
        $fm= $request->fmonth;
        $fy= $request->fyear;
        $tm= $request->tmonth;
        $ty= $request->tyear;
        $cinfo = DB::select("
SELECT 
  customers.`temple_name` AS customer,
  customers.`openning_bal` AS bal,
  SUM(stockinvoices.total_amount) AS total,
  SUM(stockinvoices.due) AS due,
  SUM(rcv_amount) AS rcv,
  SUM(discount) AS discount,
  IFNULL(refund.rf, 0) AS refund 
FROM
  stockinvoices 
  JOIN customers 
    ON stockinvoices.`customer_id` = customers.`id` 
  LEFT JOIN 
    (SELECT 
      SUM(refunditems.`amount`) AS rf,
      customer_id 
    FROM
      refunditems 
    WHERE refund_date BETWEEN '$fy-$fm-01' 
      AND '$ty-$tm-31' 
    GROUP BY refunditems.customer_id) AS refund 
    ON stockinvoices.`customer_id` = refund.`customer_id` 
WHERE DATE BETWEEN '$fy-$fm-01' 
  AND '$ty-$tm-31' 
GROUP BY stockinvoices.`customer_id` 
        ");
        return view('stock.customer.status',['infos'=>$cinfo]);
    }
}
