<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Para;
use App\PurchasePara;

class ParaController extends Controller
{  
     public function index() {
        $paras = Para::orderBy('created_at', 'desc')
            ->paginate(8);
        return view('stock.paras.paralist',compact('paras'));
    }
    public function para()
    {
        return view('stock.paras.addpara');
    }

    public function addpara(Request $request)
    {   
         //validate the data
          $validator = Validator::make($request->all(), [
            'para_name' => 'required|unique:paras|min:4|max:255',
            'p_from' => 'required',
            'origin' => 'required',
            'opn_pcs' => 'required|numeric',
        ]);
         if ($validator->fails()) {
            return Redirect::to(route('para'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $para = new Para;
        $para->para_name = $request->para_name;
        $para->p_from=$request->p_from;
        $para->origin=$request->origin;
        $para->opn_pcs=$request->opn_pcs;
        $para->save();
        return Redirect::to(route('para'))->withStatus('para successfully save');
    }
    public function purchasepara()
    {   $paras = DB::select('select id,para_name from paras');
        return view('stock.paras.purchasepara',['paras'=>$paras]);
    }
    public function addpurchasepara(Request $request)
    {    $validator = Validator::make($request->all(), [
            'item_id' => 'required',
            'pcs' => 'required|numeric',
            'purchases_price' => 'required',
            'sales_price' => 'required|numeric',
            'date' => 'required|date',
            'challan_no' => 'required|numeric',
        ],['para_id.required'=>'Please Selecet A Para From List']);
         if ($validator->fails()) {
            return Redirect::to(route('purchasepara'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $purchasePara = new PurchasePara;
        $purchasePara->item_id = $request->item_id;
        $purchasePara->pcs = $request->pcs;
        $purchasePara->item_type = $request->item_type;
        $purchasePara->challan_no = $request->challan_no;
        $purchasePara->date = $request->date;
        $purchasePara->purchases_price = $request->purchases_price;
        $purchasePara->sales_price=$request->sales_price;
        $purchasePara->save();
        $message = "Para Seccessfully Added ";
        return Redirect::to(route('purchasepara'))->withMessage($message);
    }
}
