<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Book;
use Session;
class BookController extends Controller
{   public function index() {
        $books = Book::orderBy('created_at', 'desc')
            ->paginate(8);
        return view('stock.books.bookindex',compact('books'));
    }
    public function book()
    {
        return view('stock.books.addbook');
    }
    public function addbook(Request $request){
        //validate the data
          $validator = Validator::make($request->all(), [
            'book_name' => 'required|unique:books|min:4|max:255',
            'writer' => 'required',
            'origin' => 'required',
            'opn_pcs' => 'required|numeric',
        ]);
         if ($validator->fails()) {
            return Redirect::to(route('book'))
                        ->withErrors($validator)
                        ->withInput($request->all());
        }
        
        $book = new Book;
        $book->book_name = $request->book_name;
        $book->writer=$request->writer;
        $book->origin=$request->origin;
        $book->opn_pcs=$request->opn_pcs;
        $book->save();
        return Redirect::to(route('book'))->with('status','Book Successfully Save');
    }
}
