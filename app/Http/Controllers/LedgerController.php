<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Ledger;
use Validator;
class LedgerController extends Controller
{   
    public function index() {
        $ledgers = Ledger::orderBy('created_at', 'desc')
            ->paginate(8);
        return view('accounts.ledindex',compact('ledgers'));
    }
    public function led()
    {
        return view('accounts.addledger');
    }
    public function addled(Request $request)
    {   
        //validate the data
          $validator = Validator::make($request->all(), [
            'led_name' => 'required|unique:ledgers'
        ]);
         if ($validator->fails()) {
            return Redirect::to(route('led'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $ledger= new Ledger;
        $ledger->led_name=$request->led_name;
        $ledger->led_type=$request->led_type;
        $ledger->op_bal=$request->op_bal;
        $ledger->save();
        return Redirect::to(route('led'))->with('status','Ledger Information Successfully Save');
    }
}
