<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class ContraController extends Controller
{
    public function contra() {
    $tra =DB::table('contras')->max('inv_gen');
        $departments = DB::select('SELECT id,dep_name from departments ORDER BY  dep_name ASC');
         $ledgers = DB::table('ledgers')
         ->where('led_type','cash')
         ->orWhere('led_type','bank')
         ->orderBy('led_name', 'asc')->get();
        return view('accounts.contra',['departments'=>$departments,'ledgers'=>$ledgers,'tra'=>$tra]);
    }

    public function addcontra(Request $request) {
        $transectionsdata = $request->all();
        $num_elements = 0;
        $transections = array();
        while($num_elements < count($transectionsdata['dep_id'])){
            $transections[] = array(
                't_type'=>'c',
                'dep_id' => $transectionsdata['dep_id'][$num_elements],
                'led_id'   => $transectionsdata['led_id'][$num_elements],
                'description' => $transectionsdata['description'][$num_elements],
                'dr'  => $transectionsdata['dr'][$num_elements],
                'cr'  => $transectionsdata['cr'][$num_elements],
                'date' => $transectionsdata['date'],
                'tra_num' => $transectionsdata['tra_num']
                
            );
            $cjen = ['inv_gen'=>$transectionsdata['inv_gen']];
            $num_elements++;
        }
        DB::table('transections')->insert($transections);
         DB::table('contras')->insert($cjen);
        return Redirect::to(route('contra'))->with('message','Contra Successfully Posted');
    }
}
