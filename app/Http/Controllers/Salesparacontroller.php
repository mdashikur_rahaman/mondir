<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
class Salesparacontroller extends Controller
{
    public function salepara(){
        $inv =DB::table('parainvoices')->max('id');
        $customers = DB::select('SELECT id,temple_name from customers');
        $paras = DB::table('paras')
        ->join('purchaseitems', 'paras.id', '=', 'purchaseitems.item_id')
        ->select('paras.id','paras.para_name', 'purchaseitems.sales_price')
        ->where('item_type', '=', 'para')
         ->get();
        return view('stock.paras.saleparas',['customers'=>$customers,'paras'=>$paras,'inv'=>$inv]);
    }

     public function addsalepara(Request $request){
    //save every book data
     $invoiceid =$request->inv_gen;
       $saledata = $request->all(); 
       $catchId =  $request->item_id;
       $id=[];
       for($i=0;$i<count($catchId);$i++){
        $temp = explode("|",$catchId[$i]);
        array_push($id,$temp[1]);
       }
        $num_elements = 0;
        $sqlData = array();
        while($num_elements < count($saledata['item_id'])){
            $sqlData[] = array(
                'customer_id'         => $saledata['customer_id'],
                'inv_num'          => $saledata['inv_num'],
                'item_id'     => $id[$num_elements],
                'item_type'=>'para',
                'date'=>$saledata['date'],
                'pcs'     => $saledata['pcs'][$num_elements],
                'price'     => $saledata['price'][$num_elements],
            );
            $num_elements++;
        }
        DB::table('stocksales')->insert($sqlData);
        DB::table('parainvoices')->insert(['gen'=>$invoiceid]);
       // save invoice data
       $invoice = array (
           'customer_id'         => $saledata['customer_id'],
           'item_type'  =>'para',
            'inv_num'          => $saledata['inv_num'],
            'date'          => $saledata['date'],
            'pay_type'          => $saledata['pay_type'],
            'total_amount'          => $saledata['total_amount'],
            'rcv_amount'          => $saledata['rcv_amount'],
            'discount'          => $saledata['discount'],
            'due'          => $saledata['total_amount']-$saledata['rcv_amount'],
            'cheque_num'          => $saledata['cheque_num'],
       );
       DB::table('stockinvoices')->insert($invoice);
       return Redirect::to(route('salepara'))->withStatus('Sales information Successfully saved');
    }
}
