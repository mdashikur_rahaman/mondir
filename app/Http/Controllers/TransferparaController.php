<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class TransferparaController extends Controller
{   
     public function tp() {
        $customers = DB::select('SELECT id,temple_name from customers ORDER BY temple_name ASC');
        $paras = DB::select('SELECT id,para_name from paras ORDER BY para_name ASC');
        return view('stock.paras.transferpara',['customers'=>$customers,'paras'=>$paras]);
    }
    public function atp(Request $request) {
         $tparas = $request->all(); 
         $num_elements = 0;
         $sqlData = array();
        while($num_elements < count($tparas['para_id'])){
            $sqlData[] = array(
                'item_id'     => $tparas['para_id'][$num_elements],
                'pcs'     => $tparas['pcs'][$num_elements],
                'item_type'=>'para'
            );
            $num_elements++;
        }
        DB::table('transferitems')->insert($sqlData);
    }
}
