<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class JournalController extends Controller
{
    public function journal() {
        $tra =DB::table('journals')->max('inv_gen');
        $departments = DB::select('SELECT id,dep_name from departments ORDER BY  dep_name ASC');
         $ledgers = DB::select('SELECT id,led_name from ledgers ORDER BY led_name ASC');
        return view('accounts.journal',['departments'=>$departments,'ledgers'=>$ledgers,'tra'=>$tra]);
    }

    public function addjournal(Request $request) {
        $transectionsdata = $request->all();
        $num_elements = 0;
        $transections = array();
        while($num_elements < count($transectionsdata['dep_id'])){
            $transections[] = array(
                't_type'=>'j',
                'dep_id' => $transectionsdata['dep_id'][$num_elements],
                'led_id'   => $transectionsdata['led_id'][$num_elements],
                'description' => $transectionsdata['description'][$num_elements],
                'dr'  => $transectionsdata['dr'][$num_elements],
                'cr'  => $transectionsdata['cr'][$num_elements],
                'date' => $transectionsdata['date'],
                'tra_num' => $transectionsdata['tra_num']
                
            );
            $jgen[] = ['inv_gen'=>$transectionsdata['inv_gen']];
            $num_elements++;
        }
        DB::table('transections')->insert($transections);
        DB::table('journals')->insert($jgen);
        return Redirect::to(route('journal'))->with('message','Journal Successfully Posted');
    }
}
