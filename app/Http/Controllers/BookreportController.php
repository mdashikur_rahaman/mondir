<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class BookreportController extends Controller {

public function inq() {
    return view('stock.books.reports.allbookbalq');
}

    public function all (Request $request) {
        $fm = $request->fmonth;
        $fy = $request->fyear;
        $tm= $request->tmonth;
        $ty = $request->tyear;
        $books = DB::select("
  SELECT 
  books.id,
  books.book_name,
  books.opn_pcs AS openning,
  SUM(P.pcs) AS purchase,
  IFNULL(SUM(S.sales),0) AS sales,
  IFNULL(SUM(R.refund),0) AS refund,
  IFNULL(SUM(T.wasted),0) AS wasted,
  SUM(P.purchase) AS purchase_price 
FROM
  books 
  LEFT JOIN 
    (SELECT 
      purchaseitems.`pcs` AS pcs,
      purchaseitems.`purchases_price` AS purchase,
      purchaseitems.`item_id` 
    FROM
      purchaseitems 
    WHERE purchaseitems.`item_type` = 'book' AND `purchaseitems`.`date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31')) AS P 
    ON P.`item_id` = books.`id` 
  LEFT JOIN 
    (SELECT 
      stocksales.`pcs` AS sales,
      stocksales.`item_id` 
    FROM
      stocksales 
    WHERE stocksales.`item_type` = 'book' AND `stocksales`.`date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31')) AS S 
    ON S.`item_id` = books.`id` 
  LEFT JOIN 
    (SELECT 
      refunditems.`pcs` AS refund,
      refunditems.`item_id` 
    FROM
      refunditems 
    WHERE refunditems.`item_type` = 'book' AND `refunditems`.`refund_date` BETWEEN DATE('$fy-$fm-01') AND DATE('$ty-$tm-31') ) AS R 
    ON R.`item_id` = books.`id` 
  LEFT JOIN 
    (SELECT 
      transferitems.`pcs` AS wasted,
      transferitems.`item_id` 
    FROM
      transferitems 
    WHERE transferitems.`item_type` = 'book') AS T 
    ON T.`item_id` = books.`id` 
GROUP BY books.`id` 
        ");
        return view('stock.books.reports.allbookbalance',['books'=>$books]);
    }
    
}