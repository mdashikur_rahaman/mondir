<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class DepReportController extends Controller
{
    public function show() {
        return view('accounts.reports.depwise');
    }
    public function collectdata(Request $request) {
    
     $datas = DB::table('journals')
        ->join('departments', 'departments.id', '=', 'journals.dep_id')
        ->select('departments.dep_name',DB::raw('sum(journals.dr) as dr'), DB::raw('sum(journals.cr) as cr'))
        ->where([[DB::raw('month(journals.date)'),'=',$request->month],[DB::raw('year(journals.date)'),'=',$request->year],['departments.id','=','2']])
        ->get();
    dd($datas); 
    }
}
