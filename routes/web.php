<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});
Route::get('/home', function () {
    return view('dashboard');
});
Auth::routes();

Route::group(['middleware' => 'auth','prefix' => 'stock'], function ()
{
    Route::get('gsdgsgdsassssagas','StockController@customer')->name('customer');
    Route::get('book','BookController@book')->name('book');
    Route::get('booklist','BookController@index')->name('booklist');
    Route::post('addbook','BookController@addbook')->name('addbook');
    Route::get('purchasebook','StockController@purchasebook')->name('purchasebook');
    Route::post('addpurchasebook','StockController@addpurchasebook')->name('addpurchasebook');
    Route::post('addcustomer','StockController@addcustomer')->name('addcustomer');
    // Para controller
    Route::get('para','ParaController@para')->name('para');
    Route::get('paralist','ParaController@index')->name('paralist');
    Route::post('addpara','ParaController@addpara')->name('addpara');
    Route::get('purchasepara','ParaController@purchasepara')->name('purchasepara');
    Route::post('addpurchasepara','ParaController@addpurchasepara')->name('addpurchasepara');
   // sales book controller
   Route::get('salebook','Salesbookcontroller@salebook')->name('salebook');
    Route::post('addsalebook','Salesbookcontroller@addsalebook')->name('addsalebook');
    // sales para controller
   Route::get('salepara','Salesparacontroller@salepara')->name('salepara');
   Route::post('addsalepara','Salesparacontroller@addsalepara')->name('addsalepara');
    //refund of book
   Route::get('refundbook','RefundbookController@rb')->name('rb');
   Route::post('addrefundbook','RefundbookController@arb')->name('arb');
   //refund of Para
   Route::get('refundpara','RefundparaController@rp')->name('rp');
   Route::post('addrefundpara','RefundparaController@arp')->name('arp');
   //Waste  of book
   Route::get('wastebook','TransferbookController@tb')->name('tb');
   Route::post('addwastebook','TransferbookController@atb')->name('atb');
    //Waste  of para
   Route::get('wastepara','TransferparaController@tp')->name('tp');
   Route::post('addwastepara','TransferparaController@atp')->name('atp');
   // book report
   Route::get('abl','BookreportController@inq')->name('bookinq');
   Route::post('allbookbalance','BookreportController@all')->name('allbook');
   //para report
         Route::get('apl','ParareportController@inq')->name('parainq');
      Route::post('allparabalance','ParareportController@all')->name('allpara');
    //customer status
    Route::get('stat','CustomerStatusController@stat')->name('stat');
    Route::post('status','CustomerStatusController@status')->name('status');
    //invoice 
     Route::get('bookinvoice','InvoiceController@book')->name('bookinvoice');
     Route::get('parainvoice','InvoiceController@para')->name('parainvoice');
});

Route::group(['middleware' => 'auth','prefix' => 'account'], function ()
{
    //Department Controller
    Route::get('dep','DepartmentController@dep')->name('dep');
    Route::get('deplist','DepartmentController@index')->name('depindex');
    Route::post('adddep','DepartmentController@adddep')->name('adddep');
    //Ledger Controller
    Route::get('led','LedgerController@led')->name('led');
    Route::get('ledlist','LedgerController@index')->name('ledindex');
    Route::post('addled','LedgerController@addled')->name('addled');
    //Transection Controller
    Route::get('tran','TransectionController@tran')->name('tran');
    Route::post('addtran','TransectionController@addtran')->name('addtrans');
    //journal controller
    Route::get('journal','JournalController@journal')->name('journal');
    Route::post('addjournal','JournalController@addjournal')->name('addjournal');
    //payment controller
    Route::get('payment','PaymentController@payment')->name('payment');
    Route::post('addpayment','PaymentController@addpayment')->name('addpayment');
    //Received controller
    Route::get('received','ReceivedController@received')->name('received');
    Route::post('addreceived','ReceivedController@addreceived')->name('addreceived');
    //Contra controller
    Route::get('contra','ContraController@contra')->name('contra');
    Route::post('addcontra','ContraController@addcontra')->name('addcontra');
    // Account report Controller 
    Route::get('alldep','AccReoprtController@index')->name('alldep');
    Route::post('addalldep','AccReoprtController@alldipreport')->name('addalldep');
    Route::get('depwise','AccReoprtController@dipwise')->name('depwise');
    Route::get('ledwise','AccReoprtController@ledwise')->name('ledwise');
    Route::get('fund','AccReoprtController@fund')->name('fund');
    Route::post('fundstate','AccReoprtController@fundstate')->name('fundstate');
    Route::post('addepwise','AccReoprtController@addepwise')->name('addepwise');
    Route::post('ledgerwise','AccReoprtController@ledgerwise')->name('ledgerwise');
    // Cash Book
    Route::get('cashb','AccReoprtController@cashb')->name('cashb');
    Route::post('cashbook','AccReoprtController@cashbook')->name('cashbook');
    //bankbook
    Route::get('bankb','AccReoprtController@bankb')->name('bankb');
    Route::post('bankbook','AccReoprtController@bankbook')->name('bankbook');
});
Route::group(['middleware' => 'auth'], function ()
{
 Route::resource('devotee','DevoteeController');
});
Route::group(['middleware' => 'auth','prefix' => 'departmental'], function ()
{
 Route::get('create','DepartmentalController@inf')->name('depcreate');
Route::post('addinf','DepartmentalController@addinf')->name('addinf');
Route::get('trans','DepartmentalController@trans')->name('deptrans');
Route::post('addtrans','DepartmentalController@addtrans')->name('adddeptrans');
Route::get('reportq','DepartmentalController@reportq')->name('reportq');
Route::post('rtodb','DepartmentalController@rtodb')->name('rtodb');
});